import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import { ServerService } from '../server/server.service';
import { Local } from 'protractor/built/driverProviders';
import { ThrowStmt } from '@angular/compiler';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  alert: any;
  email: string = "";
  name: string = "";
  lastname: string = "";
  tel = localStorage.cel ? localStorage.cel : "";
  password: string = "";
  referido : String = "";
  correo = false;
  nombre = true;
  codigo = true;

  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController,  
    private auth: AuthService,
    public alertCtrl: AlertController,
    db: AngularFireDatabase,
    private server: ServerService,
    private loadingctrl : LoadingController,
    private firebaseAnalytics: FirebaseAnalytics) {}

  ngOnInit() {}
  /* FirebaseAnalytics */
  analitics(){
    this.firebaseAnalytics.logEvent('checkin', {page: "payload-checkout"})
  .then((res: any) => console.log(res))
  .catch((error: any) => console.error(error));
  }
  /* Ir a login */
  goLog(){
    this.navCtrl.navigateForward('log');
  }
  /* Ir a terminos */
  goTerminos(){
    this.navCtrl.navigateForward('terminos');
  }
  /* Ir a Intro */
  async goIntro(){
    let loading = await this.loadingctrl.create();
    loading.present().then(() => {
      if(this.correo === false){
        this.navCtrl.navigateForward('verify-tel');
        loading.dismiss();
      }
      else if(this.nombre === false){
        console.log("entro");
        this.correo = false;
        this.nombre = true;
        this.codigo = true;
        loading.dismiss();
      }
      else if(this.codigo === false){
        this.correo = true;
        this.nombre = false;
        this.codigo = true;
        loading.dismiss();
      }
    })
    //this.navCtrl.navigateBack('intro');
  }
  /* Registro Email */
  async signup() {
    this.tel = localStorage.cel;
    this.password = this.tel;
    let error: string = '';
    let hay_error : boolean = false;
    this.email = this.email.trim();
    if(this.password.length < 6){ error = 'Debes ingresar una contraseña de al menos 6 caracteres'; hay_error = true;}
    if(!this.server.validateEmail(this.email)){ error = 'Email inválido, verifica que no tenga caracteres especiales ni espacios'; hay_error = true; }
    if(this.tel.length != 10){ error = 'El número de teléfono debe tener 10 números'; hay_error = true;}
    if(this.name.length < 3){ error = 'Debes ingresar un nombre de al menos 3 caracteres'; hay_error = true;}
    if(this.lastname.length < 3){ error = 'Debes ingresar un apellido de al menos 3 caracteres'; hay_error = true;}

    console.log(this.tel.length);

    if(hay_error){
      this.server.showAlert('Error de Registro',error);
      return;
    }else{
      let NombreCompleto = this.name +" "+this.lastname;
      console.log("error", NombreCompleto);
      this.server.presentLoadingDefault("Registrando...");
      let datos = {
        email: this.email,
        password: this.password,
        username: NombreCompleto,
        tel: this.tel,
        ciudad : localStorage.ciudad
      };

      await this.auth.doRegister(datos).then(async (resp)=>{
        let usuario = {
          "email" : resp.email,
          "nombre" : resp.name,
          "uid" : resp.uid,    
          "provider_data" : 'password',
          "telefono" : this.tel
        };
        console.log(usuario);
        localStorage.uid = usuario.uid;
        localStorage.user = JSON.stringify(usuario);
        this.analitics();
        this.server.dismissLoading();
        this.navCtrl.navigateRoot("tabs/home");
      }).catch((err)=>{
        this.server.dismissLoading();
        console.log("Error", err);
        if(err.code === "auth/email-already-in-use")
          this.server.showAlert('Error de Registro',"Este correo ya se registró anteriormente, verifica!");
        else
          this.server.showAlert('Error de Registro',"Verifica tus datos e intenta nuevamente!");
      });
    }
  }
  /* Registro Facebook */
  fbLogin(){
    this.server.presentLoadingDefault("Ingresando desde Facebook");
    this.auth.doFacebookLogin().then((resp)=>{
      this.analitics();
      this.server.dismissLoading();
      this.navCtrl.navigateRoot("home")
    }).catch((err)=>{
      this.server.dismissLoading();
      this.server.showAlert("Error al Ingresar","Error al ingresar con Facebook, intente nuevamente.");
      console.log("error:",err);
    });
  }
  /* Registro Google */
  googleLogin(){
    this.server.presentLoadingDefault("Ingresando desde Google");
    this.auth.doGoogleLogin().then((resp)=>{
      this.analitics();
      this.server.dismissLoading();
      this.navCtrl.navigateRoot("home");
      }).catch((err)=>{
      this.server.dismissLoading();
      this.server.showAlert("Error al Ingresar","Error al ingresar con Google, intente nuevamente.");
      console.log("error:",err);
    });
  }

  async ValidaCorreo(){
    let loading = await this.loadingctrl.create();
    loading.present().then(() => {
      if(!this.server.validateEmail(this.email)){
        loading.dismiss();
        this.server.showAlert("Mensaje", "EL correo que ingresaste no es válido ... Verifica!");
        return;
      }
      else{
        loading.dismiss();
        this.correo = true;
        this.nombre = false;
        this.codigo = true;
      }
    }).catch(error => {
      console.log(error);
    })
  }

  async ValidaNombre(){
    let loading = await this.loadingctrl.create();
    loading.present().then(() => {
      if(!this.name || !this.lastname){
        loading.dismiss();
        this.server.showAlert("Mensaje", "Tu nombre y apellido son requeridos ... Verifica!");
        return;
      }
      else{
        loading.dismiss();
        this.correo = true;
        this.nombre = true;
        this.codigo = false;
      }
    }).catch(error => {
      console.log(error);
    })
  }

  async ValidaReferido(){
    let loading = await this.loadingctrl.create({
      message : "Validando código",
    });
    loading.present().then(() => {
      if(this.referido){
        if(String(this.referido).length != 8){
          loading.dismiss();
          this.server.showAlert("Mensaje", "El código referido debe contener 8 carácteres, por favor verifica!");
          return;
        }
        else{
          this.server.verificaReferido(this.referido).then(data => {
            if(!data['status']){
              this.server.showAlert("Oops!","Tu código referido no es valido, verifica!")
              loading.dismiss();
              return;
            }
            else{
              localStorage.setItem("referido", String(this.referido));
              this.signup();
              loading.dismiss();
              
            }
          }).catch(error => {
            console.log(error);
            loading.dismiss();
          })
        } 
      }
      else{
        loading.dismiss();
        this.signup();
      }
    }).catch(error => {
      console.log("error", error);
    })
  }
}