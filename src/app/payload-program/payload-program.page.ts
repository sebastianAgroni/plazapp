import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { ServerService } from '../server/server.service';

@Component({
  selector: 'app-payload-program',
  templateUrl: './payload-program.page.html',
  styleUrls: ['./payload-program.page.scss'],
})
export class PayloadProgramPage implements OnInit {

	dia;
	ini;
	fini;
  min;
  max;
  hourvaluesini;
  hourvaluesfini;
  hourinterval;
  diashabiles;
  diashabilesWS;
  error = true;
  error_msg = '';
  bd: string = 'buttonDay';
  bds: string = 'buttonDay selected';
  selected = 0;
  selectedHour:string= "";
  fecha;
  hora;
  rangos;

  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController,
    public server: ServerService,
    public loadingCtrl : LoadingController,
    private auth: AuthService) {
    var tzoffset = (new Date()).getTimezoneOffset() * 60000;
    localStorage.setItem("descuento", "0");
    localStorage.removeItem("cupon");
  	//this.dia = new Date(Date.now() - tzoffset).toISOString().slice(0, -1);
  	//this.ini = new Date(Date.now() - tzoffset).toISOString().slice(0, -1);
  	let now = new Date(Date.now() - tzoffset).getTime();
  	//let oneHoursIntoFuture = new Date(now + 1000 * 60 * 60 * 1);
  	//this.fini = oneHoursIntoFuture.toISOString().slice(0, -1);

    var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
    var firstDate = new Date(y, m, 1);
    var lastDate = new Date(y, m + 2, 0);

    ///firstDate.setDate(firstDate.getDate()+2);

    this.min = firstDate.toISOString().slice(0, -1);
    this.max = lastDate.toISOString().slice(0, -1);

    console.log("min", this.min)
    console.log("max", this.max)


    //Get setting for days
    let tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Settings";
    this.auth.getGenKeyVal(tabla,'llave','dias_habiles').then(data => {
      let indata: any;
      indata = data;
      console.log(indata);
      this.diashabiles = indata[0].valor;
    });

    this.auth.getGenKeyVal(tabla,'llave','tiempo_minimo').then(data => {
      let indata: any;
      indata = data;
      this.hourinterval = indata[0].valor;
    });

    this.auth.getGenKeyVal(tabla,'llave','horas_habiles').then(data => {
      let indata: any;
      indata = data;
      let mininidata = indata[0].valor.split(',');

      let databuilder='';
      let mininidata2: any = parseInt(mininidata[0])+parseInt(this.hourinterval)-1;
      for (var i = mininidata2; i < mininidata[1]; i++) {
        databuilder += (i-1)+',';
        this.hourvaluesini = databuilder;
      }

      databuilder='';
      
      for (var i = mininidata2+1; i <= mininidata[1]; i++) {
        databuilder += i+',';
        this.hourvaluesfini = databuilder;
      }
    });

    this.CargaDias();
  }

  ngOnInit() {
  }


  checkDate() {
    var tzoffset = (new Date()).getTimezoneOffset() + 1  * 60000;
    console.log("tzoffset", tzoffset);
    //let now = (new Date(Date.now() - tzoffset).getTime());
    //let dianow = (new Date(this.dia).getTime());
    /****/
    let diahoy = (new Date(Date.now() - tzoffset));
    let diha = new Date(this.dia);
    let diaselect = new Date(diha+' '+diahoy.getHours()+':'+diahoy.getMinutes()+':'+diahoy.getSeconds()+':'+diahoy.getMilliseconds());
    /**/
    let houri = new Date(this.dia+' '+this.ini).getHours();
    let hourf = new Date(this.dia+' '+this.fini).getHours();
    let hourint = hourf-houri;
    /**/
    console.log("Fecha diferencia",diha.getTime()-diahoy.getTime());
    console.log("hkasdhgasdghasjhgdjahsd", diha.getTime()+" ------ "+diaselect.getDate()+" --- "+diahoy.getTime())
    if((diha.getTime()-diahoy.getTime())<0){
      this.error = true;
      this.error_msg = 'La fecha seleccionada es menor a la fecha actual.';
    } else if((diaselect.getTime()-diahoy.getTime())==0 && houri<diahoy.getUTCHours()) {
      this.error = true;
      this.error_msg = 'La hora inicial seleccionada para hoy es menor a la hora actual.';
    //} else if(isNaN(hourint)){
      //this.error = true;
      //this.error_msg = "La horas seleccionadas es menor al intervalo minimo: "+this.hourinterval+" Horas.";
    } else if((hourint)<this.hourinterval) {
      this.error = true;
      this.error_msg = 'La hora no han sido ajustadas';
    } else {
      this.error = false;
    }
  }

  continueCart(){
    if (this.error) {
      this.server.showAlert('Error de Programación',this.error_msg);
    } else {
      let range;
      console.log("diaaaa",this.dia);
      const hourdia = new Date(this.dia);
      console.log("new diaa",hourdia);
      const hourini = new Date(this.ini);
      const hourfini = new Date(this.fini);
      range = (hourdia.getDate()<10?'0':'') + hourdia.getDate() + '-'+ (hourdia.getMonth()+1<10?'0':'') + (hourdia.getMonth()+1) + '-' + hourdia.getFullYear() + '|' + hourini.getHours() + ':' + (hourini.getMinutes()<10?'0':'') + hourini.getMinutes() + '|' + hourfini.getHours() + ':' + (hourfini.getMinutes()<10?'0':'') + hourfini.getMinutes();
      localStorage.setItem('range-time', range);
    console.log('Range',range);
    this.navCtrl.navigateForward('payload-checkout');
    }
  }

  async continueCart2(){
    // this.server.presentLoadingDefault('Cargando Datos...');
    // setTimeout(async () => {
    //   let range = this.fecha+"|"+this.hora;
    //   localStorage.setItem('range-time', range);
    //   this.navCtrl.navigateForward('payload-checkout');
    // });
    let loading = await this.loadingCtrl.create({});

  loading.present().then(() => {
    let range = this.fecha+"|"+this.hora;
    localStorage.setItem('range-time', range);
    this.navCtrl.navigateForward('payload-owndata');
    loading.dismiss();
  });
    
  }

  goBack() {
    this.navCtrl.navigateBack('tabs/cart');
  }

  SeleccionaDia(fecha, id){
    if(this.selected>0)
    {
      let imgAn: HTMLElement = document.getElementById("id_"+this.selected);
      imgAn.setAttribute("class", "buttonDay md hydrated");
    }
    this.bd = "buttonDay";
    let imgEl: HTMLElement = document.getElementById("id_"+id);
    imgEl.setAttribute("class", "buttonDay selected md hydrated");
    this.selected = id;
    this.fecha = fecha;
  }

  seleccionaHora(hora,id, ValorDomicilio){
    let imgAn: HTMLElement = document.getElementById(this.selectedHour);
    imgAn.setAttribute("class", "buttonDay md hydrated");

    let imgEl: HTMLElement = document.getElementById("hora"+id);
    imgEl.setAttribute("class", "buttonDay selected md hydrated");
    this.selectedHour = "hora"+id;
    this.hora = hora;
    console.log("ValorDomicilio", ValorDomicilio);
    console.log("hora", hora);
    localStorage.setItem('envio', ValorDomicilio);
  }

  async CargaDias(){
    let loading = await this.loadingCtrl.create({});

    loading.present().then(() => {
      var idCiudad = localStorage.ciudad;
      this.server.getDays(idCiudad).then((data) => {
        if(data['ok'])
        {
          console.log("Los días hábiles", data['res']);
          this.diashabilesWS =  data['res'];
          this.selected = this.diashabilesWS[0].diaNumero;
          this.selectedHour = "hora0";
          this.rangos = data['horas'];
          this.hora = this.rangos[0].Rango;
          this.fecha = this.diashabilesWS[0].fecha;
          localStorage.setItem("envio", this.rangos[0].ValorDomicilio);
          loading.dismiss();
          return;
        }
      });
    });
  }

} 