import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import {ServerService} from '../server/server.service'
import {AuthService} from '../services/auth.service'
import { ToastController } from '@ionic/angular';
import { ThrowStmt } from '@angular/compiler';



@Component({
  selector: 'app-tipo-pago',
  templateUrl: './tipo-pago.page.html',
  styleUrls: ['./tipo-pago.page.scss'],
})
export class TipoPagoPage implements OnInit {
  tarjetaselected = 'Tarjeta Crédito';
  methodtipo = localStorage.getItem("method");
  Mediospago;
	datamethod = [
		{checked: false},
		{checked: false},
    {checked: false},
    {checked: false},
    {checked: false},
    {checked: false},
    {checked: false},
    {checked: false},
    {checked: false},
    {checked: false},
    {checked: false},
  ];
  propina = !localStorage.getItem("propina")? '0' : localStorage.getItem("propina");
  varpropina;
  varbolsa;
  otherpropina = 'Otro';
  bolsa = !localStorage.getItem("bolsa")? '0' : localStorage.getItem("bolsa");
  valor_bolsa = '0';
  valor_bolsa_setting = 0;
  referido : String;

  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController,
    private alertCtrl: AlertController,
    private server : ServerService,
    private loadingCtrl : LoadingController,
    public toastController: ToastController,
    public auth: AuthService,
    ) {
  }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    await this.getMediospago();
    await this.getPropina();
    await this.getBolsa();
    const methodactual = parseInt(localStorage.getItem('method'));
    console.log("metodo actual",methodactual);
  	if (!isNaN(methodactual)) {
  		this.datamethod[methodactual].checked = true;
      if (methodactual == 2) {
        if (localStorage.getItem("tarjeta_actual") === 'undefined' || localStorage.getItem("tarjeta_actual") === null){
          localStorage.setItem('method',"0");
          this.datamethod[0].checked = true;
        } else {
          const tarjetanombre = localStorage.getItem('tarjeta_nombre').split('|');
          this.tarjetaselected = tarjetanombre[1] + ' ' + tarjetanombre[0];
          this.methodtipo = tarjetanombre[1];
          console.log(this.tarjetaselected);
        }
      }
      if (methodactual == 1) {
        localStorage.setItem('method',"1");
      }
    }
  }

  ionViewDidEnter(){
    const methodactual = parseInt(localStorage.getItem('method'));
    if (methodactual == 2) {
      document.getElementById("Metodo_2").innerText = this.tarjetaselected;
    }
  }

  getMediospago(){
    let tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Mediopago";
    console.log(tabla);
    this.auth.getGenKeyVal(tabla,"Estado", true).then( data => {
      this.Mediospago = data;
    })
  }

  getPropina(){
    let tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Settings";
    console.log(tabla);
    this.auth.getGenKeyVal(tabla,"llave", "propina").then( data => {
      this.varpropina = data[0].valor;
    })
  }

  getBolsa(){
    let tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Settings";
    console.log(tabla);
    this.auth.getGenKeyVal(tabla,"llave", "bolsa").then( data => {
      this.varbolsa = data[0].valor;
    })
  }

  Selection(event: string) {
    console.log(event);
	   this.datamethod.forEach(x => { x.checked = false; });
     console.log('event', event);
      localStorage.setItem('method', event);
      if (event == '2') {
      localStorage.setItem('fromCart', 'true');
      this.navCtrl.navigateForward('tarjetas');
      } else {
      //this.navCtrl.navigateBack('payload-checkout');
      }
  }
  
  setPropina(value: string) {
    this.propina = value;
    const propinacheck = parseInt(value);
    if (propinacheck > 1000) {
      this.presentAlertPropina();
    } else {
      this.otherpropina = 'Otro';
      localStorage.setItem('propina', value);
    }
  }

  setBolsa(value: string) {
    this.bolsa = value;
    localStorage.setItem('bolsa', value);
    if (value == '0') {
      this.valor_bolsa = '0';
    } else {
      this.valor_bolsa = String(this.valor_bolsa_setting);
    }
  }

  async presentAlertPropina() {
    const alert = await this.alertCtrl.create({
      header: 'Ingresa tu propina',
      inputs: [
      {
        name: 'otherpropina',
        placeholder: '$0'
      }
    ],
      buttons: [
      {
        text: 'Cancelar',
        role: 'cancelar',
        handler: data => {
          console.log('Cancel clicked');
          this.propina = '1000';
          localStorage.setItem('propina', '1000');
        }
      },
      {
        text: 'Guardar',
        handler: data => {
          this.propina = data.otherpropina;
          this.otherpropina = '$' + data.otherpropina;
          localStorage.setItem('propina', data.otherpropina);
        }
      }
      ]
    });

    await alert.present();
  }

  async Checkout(){
    let loading = await this.loadingCtrl.create({});

    loading.present().then(() => {
      if(this.referido){
        if(String(this.referido).length != 8){
          loading.dismiss();
          this.server.showAlert("Mensaje", "Código referido debe terner 8 carácteres, verifica!");
          return;
        }

        if(this.referido === localStorage.uid.substr(5,8)){
          loading.dismiss();
          this.server.showAlert("Mensaje", "Código referido incorrecto, verifica!");
          return;
        }

        this.server.VerificaCodigo(localStorage.uid, this.referido).then(d => {
          if(d['ok']=== false){
            loading.dismiss();
            this.server.showAlert("Mensaje", d["mensaje"]);
            return;
          }
          else{
            this.VerificaDatos(d['mensaje']);
            localStorage.setItem("referido", String(this.referido));
            loading.dismiss();
          }
        }).catch(error => {
          loading.dismiss();
            this.server.showAlert("Mensaje", "Error");
            return;
        })
      }
      else{
        this.VerificaDatos(null);
        loading.dismiss();
      }
    });
  }

	goBack() {
    //this.navCtrl.navigateBack('payload-checkout');
    this.navCtrl.navigateBack('payload-owndata');

  }

  async VerificaDatos(msj){
    localStorage.bolsa = this.bolsa;
    localStorage.propina = this.propina;
    let tarjeta_actual = localStorage.getItem('tarjeta_actual');
    if(!localStorage.method || localStorage.method === "0"){
      this.server.showAlert("Mensaje", "Debes seleccionar un medio de pago");
      return;
    }
    if(localStorage.method === "2" && (tarjeta_actual === null || parseInt(tarjeta_actual) === 0)){
      this.server.showAlert("Mensaje", "Debes seleccionar una tarjeta");
      return;
    }

    if(msj){
      const toast = await this.toastController.create({
        message: msj,
        duration: 3000,
        //color: "success",
        
      });
      toast.present();
    }

    this.navCtrl.navigateBack('payload-checkout');
  }

}
