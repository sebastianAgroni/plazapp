import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { UserService } from '../services/user-service';
import { ServerService } from '../server/server.service';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-payload-done',
  templateUrl: './payload-done.page.html',
  styleUrls: ['./payload-done.page.scss'],
})
export class PayloadDonePage implements OnInit {
	username;
	dir;
  range;
  uid;
  nombredia = ["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado"];
  Link = "";
  mensaje;
  method;
  usuario = JSON.parse(localStorage.user);

  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController,
    private user: UserService,
    public server : ServerService,
    private alertCtrl: AlertController,
    public loadingCtrl : LoadingController,
    private iab: InAppBrowser,
    private firebaseAnalytics: FirebaseAnalytics) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.method = String(localStorage.getItem("method"));
    console.log(this.method, "method =====");
    if(this.method === '1'){
      this.mensaje = "Pago pendiente";
    }
    else{
      this.mensaje = "Pedido Realizado";
    }
    /*clean*/
    let cart = [];
    let numcart = [];
    let unitcart = [];
    let badgecart = "0";
    let preciocart = [];
    let maduracioncart = [];
    localStorage.setItem("cart",JSON.stringify(cart));
    localStorage.setItem("numcart",JSON.stringify(numcart));
    localStorage.setItem("unitcart",JSON.stringify(unitcart));
    localStorage.setItem("badgecart",badgecart);
    localStorage.setItem("preciocart",JSON.stringify(preciocart));
    localStorage.setItem("maduracioncart",JSON.stringify(maduracioncart));

    this.eviaCorreo();
    //this.Creditos(localStorage.uid);
    
      //Get User
  	// this.user.getCurrentUser()
    // .then(user => {
    // 	this.username = user.name;
    // });

    this.username = this.usuario.nombre;

  	//Get Dir
  	// let diractual = parseInt(localStorage.getItem("address"));
  	// let dirs = JSON.parse(localStorage.getItem('list-address'));
  	// let dirselect = dirs.filter(item => item.id === diractual)[0];
  	this.dir = localStorage.Dir;
  	//Get Range Time
    let rang = localStorage.getItem("range-time");
    let rangsplit = rang.split("|");
    let dia = new Date(rangsplit[0]);
    //let ini = new Date(rangsplit[1]).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
    //let fini = new Date(rangsplit[2]).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
    //this.range = this.nombredia[dia.getDay()]+" "+ini+" a "+fini;
    this.range = this.nombredia[dia.getDay()]+" "+rangsplit[1]+" a "+rangsplit[2];
    this.range = rangsplit[0] + ' ' + rangsplit[1] + ' a ' + rangsplit[2];
  }

  eviaCorreo(){
    //this.user.getCurrentUser().then(user => {
      this.analitics();
      this.uid = localStorage.uid;
      this.server.envioCorreoConfirmacion(this.uid, localStorage.referido).then((data) => {
        localStorage.removeItem("referido");
        this.method = String(localStorage.getItem("method"));
        console.log(this.method, "method =====");
        if(this.method === '1'){
          this.Link = data['link'];
          window.open(this.Link, '_system');
        }
      });
    //});
  }

  analitics(){
    this.firebaseAnalytics.logEvent('inapp-purchase', {page: "payload-done"})
  .then((res: any) => console.log(res))
  .catch((error: any) => console.error(error));
  }

  goBack() {
    this.navCtrl.navigateRoot("historial");
  }

  AbrirVentana(){
    window.open(this.Link, '_system');
    this.navCtrl.navigateRoot('historial');
  }

  
}
