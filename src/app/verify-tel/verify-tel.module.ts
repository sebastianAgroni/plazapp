import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VerifyTelPage } from './verify-tel.page';
import { ModalIndiPage } from '../modal-indi/modal-indi.page';
import { ComponentsModule } from '../componentes/componentes.module';
import { ModalIndiPageModule } from '../modal-indi/modal-indi.module';

const routes: Routes = [
  {
    path: '',
    component: VerifyTelPage
  }
];

@NgModule({
  entryComponents : [
    ModalIndiPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    ModalIndiPageModule
  ],
  declarations: [VerifyTelPage]
})
export class VerifyTelPageModule {}
