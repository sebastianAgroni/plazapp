import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAdressGeoPage } from './add-adress-geo.page';

describe('AddAdressGeoPage', () => {
  let component: AddAdressGeoPage;
  let fixture: ComponentFixture<AddAdressGeoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAdressGeoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAdressGeoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
