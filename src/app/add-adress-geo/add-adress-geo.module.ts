import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddAdressGeoPage } from './add-adress-geo.page';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';

const routes: Routes = [
  {
    path: '',
    component: AddAdressGeoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [AddAdressGeoPage],
  providers: [
    Geolocation,
    Diagnostic
  ]
})
export class AddAdressGeoPageModule {}
