import { AfterViewInit, Component, ElementRef, OnInit, ViewChild, ɵConsole } from '@angular/core';
import { ServerService } from '../server/server.service';
import { NavController, LoadingController, AlertController, MenuController, IonSelect, Events } from '@ionic/angular';
import { UserService } from '../services/user-service';
import { AuthService } from '../services/auth.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';

declare var google;

@Component({
  selector: 'app-add-adress-geo',
  templateUrl: './add-adress-geo.page.html',
  styleUrls: ['./add-adress-geo.page.scss'],
})
export class AddAdressGeoPage implements OnInit {
  Ciudad = localStorage.NombreCiudad;
  Direccion;
  Adicional;
  Indicaciones;
  DireccionR;
  lat;
  lon;
  formatted_address;
  return_direc;
  titulo;
  subtitulo;
  ctrl = 0;
  direccionActual;
  dir_manual = 0;
  dir_actual = 1;
  errperm = false;
  @ViewChild('mapElement', {static: false}) mapNativeElement: ElementRef;
  constructor(
    public server: ServerService,
    public alertCtrl: AlertController,
    public loadingctrl: LoadingController,
    public userService: UserService,
    private auth: AuthService,
    private geolocation: Geolocation,
    public navCtrl: NavController,
    private diagnostic: Diagnostic,
  ) { }

  ngOnInit(){}

  async ionViewWillEnter() {
    //console.log(this.locationStatus(), "-------->");

    // await this.diagnostic.isLocationEnabled().then((isEnabled) => {
    //   console.log(isEnabled);
    //   if (isEnabled === false) {
    //     console.log("False ....");
    //     return;
    //   } else if (isEnabled === true) {
    //     console.log("true ... ");
    //   }
    // }).catch((e) => {
    //   console.log(e);
    // });

    await this.diagnostic.isLocationEnabled().then(state => {
      if(!state){
        this.server.showAlert("Mensaje","Para poder obtener tu ubicación actual es necesario que actives el GPS, verifica");
        document.getElementById("DireccionActual").innerText = "Para poder obtener tu ubicación actual es necesario que actives el GPS";
        this.dir_actual = 0;
        this.dir_manual = 1;
        this.errperm = true;
        return;
      }
    }).catch(e => { 
      console.log(e);
    });

    if(this.errperm === true){
      return;
    }

    let loading = await this.loadingctrl.create();
    loading.present().then(async () => {
      if(navigator.geolocation) {
        this.geolocation.getCurrentPosition().then((resp) => {
          console.log(resp);
          this.lat= resp.coords.latitude
          this.lon= resp.coords.longitude
          const map = new google.maps.Map(document.getElementById('mapInicial'), {
            center: {lat: parseFloat(this.lat), lng: parseFloat(this.lon)},
            zoom: 17,
            mapTypeControl: false,
            panControl: false,
            zoomControl: false,
            scaleControl: false,
            streetViewControl: false,
            overviewMapControl: false,
            rotateControl: false,
            draggable: false
          });
          let markers= [];
          let marker = new google.maps.Marker({draggable:false, position: {lat: this.lat, lng: this.lon}, map: map});
          markers.push(marker);
          var geocoder = new google.maps.Geocoder();
          geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              var address=results[0]['formatted_address'];
              var dir = address.split(",")
              localStorage.return_direc = JSON.stringify(results[0])
              document.getElementById("DireccionActual").innerText = dir[0];
              localStorage.Dir = dir[0];
              return dir[0];
            }
          });
          loading.dismiss();
        }).catch((error) => {
          console.log('Error getting location', error);
          loading.dismiss();
          this.dir_actual = 0;
          this.dir_manual = 1;
          this.errperm = true;
          // this.server.showAlert("Mensaje","Para poder obtener tu ubicación actual es necesario des permisos de ubicación, verifica");
          document.getElementById("DireccionActual").innerText = "Para poder obtener tu ubicación actual es necesario des permisos de ubicación";
        });
      }
      else{
        loading.dismiss();
        this.server.showAlert("Mensaje","Para poder obtener tu ubicación actual es necesario que actives el GPS, verifica");
        this.dir_actual = 0;
        this.dir_manual = 1;
        this.errperm = true;
      }
    });
  }

  async locationStatus() {
    return new Promise((resolve, reject) => {
       this.diagnostic.isLocationEnabled().then((isEnabled) => {
       console.log(isEnabled);
       if (isEnabled === false) {
          resolve("false");
       } else if (isEnabled === true) {
          resolve("true");
       }
     })
   .catch((e) => {
   // this.showToast('Please turn on Location');
   console.log(e);
   reject("false");
   });
  });
 }

  ionViewDidEntera(){
    this.Direccion = localStorage.Dir;
    console.log("Did");
  }

  async UbicacionActual(){
    let loading = await this.loadingctrl.create();
    loading.present().then(() => {
      if(navigator.geolocation) {
        this.geolocation.getCurrentPosition().then((resp) => {
          this.lat= resp.coords.latitude
          this.lon= resp.coords.longitude
          console.log(this.lat);
          this.ConfimaMapaInicial();
          setTimeout(async () => {
            this.PintaMapaVerifica(this.lat, this.lon);
            document.getElementById('map').style.width = "235px";
            document.getElementById('map').style.height = "280px";
            loading.dismiss();
          }, 500);
        }).catch((error) => {
          console.log('Error getting location', error);
          loading.dismiss();
          // this.server.showAlert("Mensaje","Para poder obtener tu ubicación actual es necesario que actives el GPS, verifica");
          this.dir_actual = 0;
          this.dir_manual = 1;
        });
      }
      else{
        loading.dismiss();
        this.server.showAlert("Mensaje","Para poder obtener tu ubicación actual es necesario que actives el GPS, verifica");
        this.dir_actual = 0;
        this.dir_manual = 1;
      }
    })
    
  }

  cargaDirecciones(dir){
    localStorage.Dir = dir;
    this.direccionActual = dir;
  }

  cambiaDir(){
    if(this.dir_actual == 0){
      this.dir_actual = 1;
      this.dir_manual = 0;
    }
    else{
      this.dir_actual = 0;
      this.dir_manual = 1;
    }
  }

  async ValidarDireccion(){
    if(!this.Ciudad || !this.Direccion){
      this.server.showAlert('Mensaje',"Hay campos vacíos, por favor verifica!");
      return;
    }

    
    this.DireccionR =this.Direccion+" "+this.Ciudad;
    console.log(this.DireccionR);

    let datos = {
      "uid" : localStorage.uid,
      "direccion" : this.DireccionR,
    }

    // this.server.getGeocode(localStorage.uid, this.DireccionR).then(data => {
    let loading = await this.loadingctrl.create();
    loading.present().then(() => {
      this.server.getGeocode(JSON.stringify(datos)).then(data => {
        let r = JSON.parse(JSON.stringify(data));
        if(!r.ok){
          this.server.showAlert("Mensaje", r.error_msg);
          loading.dismiss();
          return;
        }
        if(r.resp.length>1){
          this.VariosResultados(r.resp);
          loading.dismiss();
          return;
        }
        else{
          // if(r.resp[0].formattedAddress === 'Bogota, Colombia'){
          //   this.server.showAlert("Mensaje", "No pudimos encontrar tu dirección, verifica que esté escrita correctamente");
          //   loading.dismiss();
          //   return;
          // }
          // else{
            loading.dismiss();
            let result = r.resp[0]
            this.formatted_address = r.resp[0].formattedAddress;
            this.return_direc = r.resp;
            this.ctrl = 0;
            this.titulo = "¿Es tu dirección?";
            this.subtitulo = "";
            this.lat = parseFloat(r.resp[0].latitude);
            this.lon = parseFloat(r.resp[0].longitude);
            this.VerificarDireccion(this.titulo, this.subtitulo);
            setTimeout(async () => {
              this.PintaMapaVerifica(this.lat, this.lon);
              document.getElementById('map').style.width = "235px";
              document.getElementById('map').style.height = "280px";
            }, 800);
          //}
        }
      }).catch(error => {
        console.error("CATH", error);
        loading.dismiss();
      })
    });

    // this.server.geoCode(this.Direccion).then((data)=>{
    //   let resp: any = data;
    //   var res = data.results;
    //   if(res.length > 1){
    //     // this.server.showAlert('Mensaje',"hay "+res.length +" direcciones");
    //     this.VariosResultados(res);
    //     return;
    //   }
    //   else{
    //     let result;
    //     for(let r in data.results){
    //       if(data.results[r].geometry.location_type !== 'undefined' && data.results[r].geometry.location_type == "ROOFTOP"){
    //         result = data.results[r];
    //       }
    //     }
    //     if(!result)
    //       result = data.results[0];
    //     this.formatted_address = result.formatted_address;
    //     this.return_direc = result;
    //     var l = result.geometry.location;
    //     this.ctrl = 0;
    //     this.titulo = "¿Es tu dirección?";
    //     this.subtitulo = "";
    //     if(result.geometry.location_type == "APPROXIMATE"){
    //       this.ctrl = 1;
    //       this.titulo = "No encontramos tu dirección";
    //       this.subtitulo = "Mueve el puntero hasta tu ubicación";
    //     }
    //     this.lat = l.lat;
    //     this.lon = l.lng;
    //     this.VerificarDireccion(this.titulo, this.subtitulo);
    //     setTimeout(async () => {
    //       this.PintaMapaVerifica(this.lat, this.lon);
    //       document.getElementById('map').style.width = "235px";
    //       document.getElementById('map').style.height = "280px";

    //     }, 500);
    //   }
    // }).catch((err)=>{
    //     this.server.showAlert('Mensaje',"No pudimos encontrar tu dirección, verifica que esté escrita correctamente");
    // });
  }

  PintaMapaVerifica(lat, lon): void {
    const map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: parseFloat(lat), lng: parseFloat(lon)},
      zoom: 17,
      mapTypeControl: false,
      panControl: false,
      zoomControl: false,
      scaleControl: false,
      streetViewControl: false,
      overviewMapControl: false,
      rotateControl: false,
      draggable: false
    });
    let markers= [];
    let marker = new google.maps.Marker({draggable:false, position: {lat: lat, lng: lon}, map: map});
    markers.push(marker);
    google.maps.event.addListener(marker,'dragend',function(event) {
      var x = this.getPosition().lat();
    });

    if(this.ctrl === 1){
      map.setOptions({draggable: true});
      var geocoder = new google.maps.Geocoder();
      google.maps.event.addListener(map, 'center_changed', function(event){
        var lat = this.getCenter().lat();
        var lng = this.getCenter().lng();
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(null);
        }
        marker =[];
        marker = new google.maps.Marker({draggable:false, position: {lat: lat, lng: lng}, map: map});
        localStorage.lat = lat;
        localStorage.lng = lng;
        markers.push(marker);
      });
      
      google.maps.event.addListener(map, 'dragend', function(event){
        console.log(localStorage.lat, localStorage.lng);
        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            var address=results[0]['formatted_address'];
            var dir = address.split(",")
            localStorage.Dir = dir[0];
            localStorage.return_direc = JSON.stringify(results[0])
          }
        });
      });      
    }
  }

  async VerificarDireccion(titulo, subtitulo) {
    let botones = [];
    if(this.ctrl === 0){
      botones.push({
        text: 'No, seleccionar en el mapa',
        handler: data => {
          this.ctrl = 1;
          this.titulo = "Encontremos tu dirección";
          this.subtitulo = "Mueve el mapa hasta tu ubicación";
          this.VerificarDireccion(this.titulo, this.subtitulo);
          setTimeout(async () => {
            this.PintaMapaVerifica(this.lat, this.lon);
            document.getElementById('map').style.width = "235px";
            document.getElementById('map').style.height = "280px";

          }, 500);
        }
      },
      {
        text: 'Si, verificar Cobertura',
        handler: data => {
          this.GeoCode(this.formatted_address);
        }
      })
    }

    if(this.ctrl === 1){
      botones.push({
        text: 'Verificar Cobertura',
        handler: data => {
          //this.Direccion = localStorage.Dir;
          this.lat = localStorage.lat;
          this.lon =  localStorage.lng;
          this.return_direc = JSON.parse(localStorage.return_direc);
          this.GeoCode(this.formatted_address);
        }
      });
    }
    const alert = await this.alertCtrl.create({
      header: titulo,
      subHeader : subtitulo,
      message: '<div id="map" style="width: 800px!important;height: 790px!important;"></div>',
      buttons: botones
  });
    await alert.present();
  }

  GeoCode(direc){
    this.server.presentLoadingDefault('Validando Cobertura ...');
    console.log(localStorage.Dir, "localStorage.Dir");
    if(typeof this.Direccion === "undefined")
      this.Direccion = document.getElementById("DireccionActual").innerText;

    //this.userService.getCurrentUser().then(user => {
      let uid = localStorage.uid;
      if(this.Adicional)
        this.Direccion = this.Direccion.replace(/([\ \t]+(?=[\ \t])|^\s+|\s+$)/g, '');
      if(this.Adicional)
        this.Adicional = this.Adicional.replace(/([\ \t]+(?=[\ \t])|^\s+|\s+$)/g, '');
      if(this.Indicaciones)
        this.Indicaciones = this.Indicaciones.replace(/([\ \t]+(?=[\ \t])|^\s+|\s+$)/g, '');
      let dir = {
        "uid" : localStorage.uid,
        "Ciuidad": localStorage.ciudad, 
        "Direccion" : this.Direccion,
        "Adicional" : this.Adicional,
        "Indicaciones" : this.Indicaciones,
        "alias" : this.formatted_address,
        "latitud" : this.lat,
        "longitud" : this.lon,
        "return_direc" : this.return_direc,
        "UnidadNegocio" : localStorage.UnidadNegocio,
      }

      let data = JSON.stringify(dir);
      this.server.SaveDir(data).then((data)=>{
        if(!data['ok']){
          this.server.dismissLoading();
          this.server.showAlert('Mensaje',data['error_msg']);
        }
        else{
          this.server.dismissLoading();
          this.server.showAlert('Mensaje',data['error_msg']);
          // this.navCtrl.navigateRoot('tabs/home');
          setTimeout(async () => {
            this.navCtrl.navigateRoot('mis-direcciones');
          }, 1000);

        }
      }).catch((err)=>{
        this.server.dismissLoading();
        this.server.showAlert('Error Red',"Tu conexión se ha perdido, intenta nuevamente.");
      });
    //});
  }

  PintaMapaInicial(lat, lon){
    const map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: lat, lng: lon},
      zoom: 17,
      mapTypeControl: false,
      panControl: false,
      zoomControl: false,
      scaleControl: false,
      streetViewControl: false,
      overviewMapControl: false,
      rotateControl: false
    });
    const marker = new google.maps.Marker({position: {lat: lat, lng: lon}, map: map});
  }

  async ConfimaMapaInicial() {
    const alert = await this.alertCtrl.create({
      header: 'Esta es tu ubicación Actual, ¿La Agregamos?',
      message: '<div id="map" style="width: 800px!important;height: 790px!important;"></div>',
      buttons: [
      {
        text: 'Cancelar',
        role: 'cancelar',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Si, verificar Cobertura',
        handler: data => {
          this.GeoCode(this.formatted_address);
        }
      }]
  });
    await alert.present();
  }

  async VariosResultados(arr) {
    let html = [];
    for(let r in arr){
      var d = arr[r].formattedAddress;
      //.split(",");
      var val = arr[r].latitude+","+arr[r].longitude;
      html.push({name : "radio"+r, type : 'radio', label : d, value : val})
      //html += "<div (click)='PintaMapaVerifica("+arr[r].geometry.location.lat+","+arr[r].geometry.location.lng+")' class='ion-text-left ion-text-primary'>"+d[0]+"</div><br>";
      // html += '<ion-button ng-click="PintaMapaVerifica("+arr[r].geometry.location.lat+","+arr[r].geometry.location.lng+")">'+d[0]+'</ion-button>';
    }
    const alert = await this.alertCtrl.create({
      header: 'Selecciona una dirección',
      inputs: html,
      buttons: [
      {
        text: 'Cancelar',
        role: 'cancelar',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'verificar Cobertura',
        handler: data => {
          //this.GeoCode(this.formatted_address);
          var datos = data.split(",");
          console.log(data);
          console.log(parseFloat(datos[0]), parseFloat(datos[1]));
          //return;
          this.ctrl = 0;
          this.titulo = "¿Es tu dirección?";
          this.subtitulo = "";
          this.VerificarDireccion(this.titulo, this.subtitulo);
          this.lat = parseFloat(datos[0]);
          this.lon=  parseFloat(datos[1]);
          setTimeout(async () => {
            this.PintaMapaVerifica(parseFloat(datos[0]), parseFloat(datos[1]));
            document.getElementById('map').style.width = "235px";
            document.getElementById('map').style.height = "280px";

          }, 500);
          // this.PintaMapaVerifica(datos[0], datos[1]);
        }
      }]
  });
    await alert.present();
  }

  goBack(){
    this.navCtrl.navigateBack('tabs/home');
  }
}