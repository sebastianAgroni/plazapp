import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders , HttpRequest } from '@angular/common/http';
import { UserService } from '../services/user-service';
import { LoadingController, AlertController } from '@ionic/angular';
import * as firebase from 'firebase/app';

import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class ServerService {
  isLoading;
	loading;
  // //apipath = 'http://cms.plaz.com.co/api/';
  // //apipath2 = 'http://35.238.104.145/index.php/api/';
  // apipath = 'http://cms.plaz.com.co/api/';
  // apipath2 = 'http://huerto.plaz.com.co/index.php/api/';

  apipath = 'http://cms.plaz.com.co/api/';
  apipath2 = 'http://huerto.plaz.com.co/index.php/api/';
  nodepath = "http://huerto.plaz.com.co:3000/";
  nodepathLocal = "http://localhost:3000/";

  orm_path = "http://huerto.plaz.com.co:1111/"
  orm_pathLocal = "http://localhost:1111/"

  registerUser = this.apipath + 'registrar-usuario';
  updateUsuario = this.apipath + 'update-usuario';
  agregarDireccion = this.apipath + 'add-direccion';
  agregarTarjeta = this.apipath + 'agregar-tarjeta';
  eliminarDireccion = this.apipath + 'remove-direccion';
  eliminarTarjeta = this.apipath + 'remove-tarjeta';
  newPedido = this.apipath + 'secure/pedido';
  terminos = this.apipath + 'terminos';

  cupon = this.apipath2 + 'cupon';
  guarda_cupon = this.apipath2 + 'guardacupon';
  //historial_pedidos = this.apipath2 + 'mispedidos';
  //historial_pedidos_detalle = this.apipath2 + 'mispedidosdetalle';
  infouser = this.apipath2 + 'infouser';
  //Geocode = this.nodepath + 'Geocode/Colombia';
  Settings = this.nodepath + 'GetDataSymfony/Settings'
  envioCorreo = this.nodepath + 'GetDataSymfony/email'
  credito = this.nodepath + 'GetDataSymfony/getCreditos'
  GuardaCredito = this.nodepath + 'GetDataSymfony/saveCreditos'
  // TraeDias = this.nodepath + 'GetDataSymfony/getDiasHabiles'
  //  SaveDirUrl = this.nodepath + 'Geocode/SaveDir';

  GetUserByuid = this.nodepath +'GetDataSymfony/GetUserByuid';
  SetUserbyId = this.nodepath +'GetDataSymfony/SetUserbyId';
  
  verificaCodigo = this.nodepath +'GetDataSymfony/VerificaCodigo';
  validaMasterPromo = this.nodepath +'GetDataSymfony/ValidaMasterPromo';
  getTarjetas = this.nodepath +'GetDataSymfony/GetTarjetas';
  validaInventario = this.nodepath +'GetDataSymfony/validaInventario';
  
  sendSms = this.orm_path + "GetDataOrm/SendSms";
  getCiudades = this.orm_path +'GetDataOrm/GetCiudades';
  getAllDataTable = this.orm_path +'GetDataOrm/GetAllDataTable';
  createUser = this.orm_path +'SetDataOrm/CreateNewUser';
  getUserInfo = this.orm_path +'GetDataOrm/GetUserInfo';
  TraeDias = this.orm_path + 'GetDataOrm/GetDiasHabiles'
  SaveDirUrl = this.orm_path + 'Geocode/SaveDir';
  getProductosPorUN = this.orm_path + "GetDataOrm/GetProductosPorUN";
  Geocode = this.orm_path + "Geocode/Geolocaliza";
  VerificaReferido = this.orm_path + "GetDataOrm/VerificaReferido";
  AddPedidoPg = this.orm_path + "Pagos/ContraEntrega";
  addTarjeta2 = this.orm_path + "GetDataOrm/AddTarjeta";
  historial_pedidos = this.orm_path + "GetDataOrm/GetCompras";
  historial_pedidos_detalle = this.orm_path + "GetDataOrm/GetCompraDetalle";

  constructor(public loadingCtrl: LoadingController,
    public http: HttpClient,
    public userService: UserService,
    public alertCtrl: AlertController) {}


  /* Validar Codigo promo */
  ValidaCodigo(datos : any){
    console.log("Recibe para mandar al servicio", datos);
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      this.http.post(this.cupon, datos, requestOptions)
      .subscribe(data => {
        console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  /* Guarda Codigo promo */
  GuardaCodigo(datos : any){
    console.log(datos);
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      this.http.post(this.guarda_cupon, datos, requestOptions)
      .subscribe(data => {
        console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  /* Crear Usuario */
  createNewUser(register: boolean, name: string, tel: string) {
    this.userService.getCurrentUser()
      .then(user => {
        let headers = new HttpHeaders();
        headers.append('Access-Control-Allow-Origin' , '*');
        headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
        headers.append('Accept', 'application/json');
        headers.append('content-type', 'application/json');
        let requestOptions =  { headers: headers };
        if (register) {
          user.name = 'Temporal';
        }
        if(user.email === null || user.email === undefined || user.email === "null"){
          user.email = user.uid+"@facebook.com";
        }
        this.http.post(this.registerUser, user, requestOptions)
          .subscribe(data => {
            if (register) {
              user.name = name; user.telefono = tel;
              this.updateUser(user);
            }
          }, error => {
          });
      }, (err) => {
      });
  }
  /* Actualizar Usuario */
  updateUser(user: any): any {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      this.http.post(this.updateUsuario, user, requestOptions).subscribe(data => {
        console.log(data);
        let userfb = firebase.auth().currentUser;
        userfb.updateProfile({
            displayName: user.name
        });
        localStorage.setItem('phoneUser', user.telefono);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error.error);
      });
    }
    );
  }
  /* Agregar Dirección */
  addDireccion(direccion: { dir: string; adicional: string; predeterminada: number; lat: number; lng: number; uid: string; id: number; }): any {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      this.http.post(this.agregarDireccion, direccion, requestOptions)
      .subscribe(data => {
        console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }
  /* Eliminar Dirección */
  removeDireccion(datos: any) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      this.http.post(this.eliminarDireccion, datos, requestOptions)
      .subscribe(data => {
        console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }
  /* Agregar Tarjeta */
  addTarjeta(datos: any) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      this.http.post(this.agregarTarjeta, datos, requestOptions)
      .subscribe(data => {
        console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  AddTarjeta2(datos) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      let data = {
        "data" : datos
      }
      this.http.post(this.addTarjeta2, data, requestOptions)
      .subscribe(data => {
        console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }
  /* Eliminar Tarjeta */
  removeTarjeta(datos: any) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      this.http.post(this.eliminarTarjeta, datos, requestOptions)
      .subscribe(data => {
        console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }
  /* Agregar Pedido */
  addPedido(datos: any) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      this.http.post(this.newPedido, datos, requestOptions)
      .subscribe(data => {
        console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }
  /* Agregar Pedido */
  addPedidoPg(data) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        "data" : data
      };
      this.http.post(this.AddPedidoPg, datos, requestOptions)
      .subscribe(data => {
        console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }
  /* Obtener Términos */
  getTerminos() {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {};
      this.http.post(this.terminos, datos, requestOptions)
      .subscribe(data => {
        console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }
  /* Geolocalizar */
  geoCode(dir: string): any {
    return new Promise((resolve, reject) => {
      dir = dir.replace(/[\W_]+/g,' ');
      dir = dir.replace(/ /g, "+", );
      // let url = 'https://maps.googleapis.com/maps/api/geocode/json?address='+ dir + "&key=AIzaSyCpXAChhUJHccLmv3sE-FM8enJ79lkS4F0&library=geometry";
      let url = 'https://maps.googleapis.com/maps/api/geocode/json?address='+ dir + "&key=AIzaSyD5NXURVbNlpIyp5MtJYO0nbaepSo6Rtto&library=geometry";
      console.log('url', url);
      this.http.get(url).subscribe(data => {
        console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error.error);
      });
    }
    );
  }
  /* Mostrar Carga */
  async presentLoadingDefault(title: string) {
    this.loading = await this.loadingCtrl.create({
      spinner: 'circles',
      message: title,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await this.loading.present();
  }
  /* Mostrar Alerta */
  async showAlert(titulo, mensaje) {
    const alertct = await this.alertCtrl.create({
      header: titulo,
      message: mensaje,
      buttons: ['OK']
    });
    await alertct.present();
  }
  /* Descartar Carga */
  async dismissLoading() {
    setTimeout(async () => {
      return await this.loading.dismiss();
    }, 500);

  }
  /* Funcion Validar Email */
  validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  /* Obtener Términos */
  getHistorial(datos) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const data = {
        'data' : datos
      };
      this.http.post(this.historial_pedidos, data, requestOptions)
      .subscribe(data => {
        //console.log(data);
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  // Obtener Detalle Pedido 
  getHistorialDetalle(idCompra : any, UnidadNegocio : any) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        'idCompra' : idCompra,
        'UnidadNegocio' : UnidadNegocio
      };
      this.http.post(this.historial_pedidos_detalle, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  getInfoUser(uid)
  {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        'uid' : uid,
      };
      this.http.post(this.infouser, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  getUserByuid(uid)
  {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        'uid' : uid,
      };
      this.http.post(this.GetUserByuid, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  setUserbyId(data)
  {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        'data' : data,
      };
      this.http.post(this.SetUserbyId, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  // getGeocode(uid, dir)
  getGeocode(data)
  {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        "data" : data
      };
      this.http.post(this.Geocode, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  //valiAddressPay(uid, idDireccion)
  valiAddressPay(data)
  {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        "data" : data
      };
      this.http.post(this.Geocode, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  getSettings(uid)
  {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        'uid' : uid,
      };
      this.http.post(this.Settings, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  envioCorreoConfirmacion(uid, referido){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        'uid' : uid,
        'code' : referido
      };
      this.http.post(this.envioCorreo, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  getCreditos(uid){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        'uid' : uid,
      };
      this.http.post(this.credito, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  async VerificaCodigo(uid, codigo){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        'uid' : uid,
        'codigo' : codigo,
      };
      this.http.post(this.verificaCodigo, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  ValidaMasterPromo(id){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        'id' : id
      };
      this.http.post(this.validaMasterPromo, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  saveCreditos(uid, creditos, envio){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        'uid' : uid,
        'creditos' : creditos,
        'envio' : envio,
      };
      this.http.post(this.GuardaCredito, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  getDays(idCiudad){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        "idCiudad" : idCiudad
      };
      this.http.post(this.TraeDias, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  SaveDir(data){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        "data" : data
      };
      this.http.post(this.SaveDirUrl, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  GetTarjetas(uid){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        "uid" : uid
      };
      this.http.post(this.getTarjetas, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  GetCiudades(){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
      };
      this.http.post(this.getCiudades, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  async GetAllDataTable(table: String){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        "table" : table
      };
      this.http.post(this.getAllDataTable, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  ValidaInventario(data){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        "data" : data
      };
      this.http.post(this.validaInventario, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  CreateUser(data){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        "data" : JSON.stringify(data)
      };
      this.http.post(this.createUser, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  GetUserInfo(uid){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        "uid" : uid
      };
      this.http.post(this.getUserInfo, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  GetCountries(){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {};
      this.http.get("https://restcountries.eu/rest/v2/all?fields=alpha3Code;alpha2Code;callingCodes;flag;name;", datos)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  GetProductosPorUN(idCliente){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        "idCliente" : idCliente
      };
      this.http.post(this.getProductosPorUN, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  verificaReferido(codigo){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        "codigo" : codigo
      };
      this.http.post(this.VerificaReferido, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  SendSms(data){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        "data" : JSON.stringify(data)
      };
      this.http.post(this.sendSms, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }

  goBack() {
    //this.navCtrl.pop();
  }
}
