import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { ParametrosService } from '../services/parametros.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.page.html',
  styleUrls: ['./blog.page.scss'],
})
export class BlogPage implements OnInit {

  post: any;

  constructor(public navCtrl: NavController, public menuCtrl: MenuController, private auth: AuthService,public param: ParametrosService) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.getPost();
  }

  getPost() {
    let me = this;
    var tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Post";
    this.auth.getGenOV(tabla).then(data => {
      let posts: any;
      posts = data;
      this.post = posts.filter(item => item.categoria === 4);
      localStorage.setItem('blog',JSON.stringify(data));
      console.log("Post: ",this.post);
    })
  }

  goEntrada(id: any){
    this.param.dataPost(id);
  	this.navCtrl.navigateForward("entrada-blog");
  }

  openMenu() {
    this.menuCtrl.toggle();
  }

}
