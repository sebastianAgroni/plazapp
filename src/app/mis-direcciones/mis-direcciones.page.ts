import { Component, OnInit } from '@angular/core';

import { NavController, LoadingController, AlertController, MenuController, IonSelect, Events } from '@ionic/angular';

import { AuthService } from '../services/auth.service';
import { ServerService } from "../server/server.service";
import { UserService } from '../services/user-service';
import { type } from 'os';

@Component({
  selector: 'app-mis-direcciones',
  templateUrl: './mis-direcciones.page.html',
  styleUrls: ['./mis-direcciones.page.scss'],
})
export class MisDireccionesPage implements OnInit {
  fromCartNav = false;
	direcciones: any;
	datos = {
    "uid": "",
    "id": ""
  };

  constructor(public alertCtrl: AlertController,
    private auth: AuthService, 
    public menuCtrl: MenuController, 
    public server: ServerService, 
    public nav: NavController,
    public userService: UserService,
    public navCtrl: NavController,
    public events: Events) {
  	this.getDirs();
  }

  ngOnInit() {
  }

  openMenu() {
    this.menuCtrl.toggle();
  }

  ionViewWillEnter(){

  }

  getDirs(){
    // this.userService.getCurrentUser()
    // .then(user => {
      let tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Envio";
      this.auth.getGenKeyVal(tabla,"uid",localStorage.uid).then(data => {
        this.direcciones = data;
        localStorage.setItem("list-address",JSON.stringify(data));
        console.log("Direcciones: ",data);
        let address = localStorage.getItem("address");
        let pre_address = null;
        if(typeof address !== 'undefined'){
          pre_address = address.split("~")[0];
        }
        if (pre_address!=null) {
          for (var i = 0; i < this.direcciones.length; i++) {
            if(this.direcciones[i].id==pre_address) {
              this.direcciones[i].checked = true; 
              
            } else {
              this.direcciones[i].checked = false;
            }
          }
        }
      }).catch( error => {
        console.log("error", error);
      })
    // });
  }

  goBack() {
    this.navCtrl.navigateRoot("tabs/home");
    //location.href = "tabs/home";
  }

  deleteDireccion(id: any){
  		this.server.presentLoadingDefault("Eliminando...");
      let user = this.auth.getUser();
      this.datos.uid = user.uid;
      this.datos.id = id;
      this.server.removeDireccion(this.datos).then((data)=>{
        this.server.dismissLoading();
        this.server.showAlert("Dirección Eliminada","Dirección eliminada con éxito");
        this.navCtrl.navigateBack("tabs/home");
      }).catch((err)=>{
        this.server.dismissLoading();
        this.server.showAlert("Error al Eliminar","Ha ocurrido un error al eliminar dirección");
      });
  }

  addDireccion(){
    this.nav.navigateBack("add-adress-geo");
    //this.nav.navigateBack("tabs/home");
    //this.events.publish('user:open');
  }

  Selection(item: any) {
    this.direcciones.forEach(x => { x.checked = false });
    localStorage.setItem("address",item.id+"~"+item.direccion);
 }

}
