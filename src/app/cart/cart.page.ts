import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { ParametrosService } from '../services/parametros.service';
import { ServerService } from '../server/server.service';

import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'], 
})
export class CartPage implements OnInit { 
  tallas: any;
  cart: any;
  numcart: any;
  unitcart: any;
  maduracioncart: any;
  unittallacart: any;
  preciocart: any;
  tempproductos: any = [];
  productos: any;
  finalproductos = [];
  envio = 10000;
  total = 0;
  subtotal = 0;
  unitcarro: any = [];
  ctrl = 0
  Link = "";
  inventario;
  Faltantes = "";
  moneda = localStorage.moneda;

  constructor(public navCtrl: NavController,
    public param: ParametrosService,
    public menuCtrl: MenuController,
    public alertctrl : AlertController,
    public server: ServerService,
    public loadingCtrl: LoadingController,
    private auth: AuthService,) { }

  ngOnInit() {
    this.finalproductos = [];
  }

  async ionViewWillEnter() {
    // let loading = await this.loadingCtrl.create({
    //   message: 'Verificando tus productos',
    // });
    // loading.present().then(() => {
    // Get List of Tallas
      console.log(localStorage, "uid");
      this.finalproductos = [];
      this.ctrl = 0;
      let tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Talla";
      this.auth.getGenOV(tabla).then(data => {
        this.tallas = data;
        console.log('talas', this.tallas);
      });
      // Get setting for envio
      tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Settings";
      this.auth.getGenKeyVal(tabla,'llave','valor_domicilio').then(data => {
        let indata: any;
        indata = data;
        console.log('envio', indata[0].valor);
        this.envio = parseInt(indata[0].valor);
        if(localStorage.UnidadNegocio === "2"){
          this.filterProductos();
        }
        else{
          this.filterProductos2();
        }
        this.calculateTotal();
      });
      // Get carts values
      this.cart = JSON.parse(localStorage.getItem('cart'));
      this.numcart = JSON.parse(localStorage.getItem('numcart'));
      this.unitcart = JSON.parse(localStorage.getItem('unitcart'));
      this.unittallacart = JSON.parse(localStorage.getItem('unittallacart'));
      this.preciocart = JSON.parse(localStorage.getItem('preciocart'));
      this.maduracioncart = JSON.parse(localStorage.getItem('maduracioncart'));
      this.productos = JSON.parse(localStorage.getItem('productos'));

      console.log('unit', this.unitcart);
      console.log('num', this.numcart);
    //   loading.dismiss();
    // });

  }

  calculateTotal() {
    this.subtotal = 0;
    for (let i = 0; i < this.preciocart.length; i++) {
      this.subtotal = this.subtotal + parseInt(this.preciocart[i]);
    }
    console.log('subtotal+envio', this.subtotal + " - " + this.envio);
    this.total = this.subtotal + this.envio;
    localStorage.setItem('subtotal', String(this.subtotal));
    localStorage.setItem('envio', String(this.envio));
  }

  removeCart(index: any) {
  	console.log('id remove:', index);
  	this.cart = JSON.parse(localStorage.getItem('cart'));
    this.numcart = JSON.parse(localStorage.getItem('numcart'));
    this.unitcart = JSON.parse(localStorage.getItem('unitcart'));
    this.unittallacart = JSON.parse(localStorage.getItem('unittallacart'));
    this.preciocart = JSON.parse(localStorage.getItem('preciocart'));
    this.maduracioncart = JSON.parse(localStorage.getItem('maduracioncart'));
    this.unitcarro = JSON.parse(localStorage.getItem('unitcarro'));
  	this.cart.splice(index, 1);
  	this.numcart.splice(index, 1);
    this.unitcart.splice(index, 1);
    this.unittallacart.splice(index, 1);
    this.preciocart.splice(index, 1);
    this.maduracioncart.splice(index, 1);
    // this.unitcarro.splice(index, 1);
  	localStorage.setItem('cart', JSON.stringify(this.cart));
    localStorage.setItem('numcart', JSON.stringify(this.numcart));
    localStorage.setItem('unitcart', JSON.stringify(this.unitcart));
    localStorage.setItem('unittallacart', JSON.stringify(this.unittallacart));
    localStorage.setItem('preciocart', JSON.stringify(this.preciocart));
    localStorage.setItem('maduracioncart', JSON.stringify(this.maduracioncart));
    localStorage.setItem('unitcarro', JSON.stringify(this.unitcarro));
    console.log('cart', this.cart);
    console.log('numcart', this.numcart);
    console.log('unitcart', this.unitcart);
    console.log('unittallacart', this.unittallacart);
    console.log('preciocart', this.preciocart);
    console.log('maduracioncart', this.maduracioncart);
    console.log('unitcarro', this.unitcarro);
    this.filterProductosRemove();
    this.calculateTotal();
    this.reloadCart();
  }

  filterProductos2() {
  	this.finalproductos = [];
    for (let i = 0; i < this.cart.length; i++) {
      this.finalproductos[i] = this.productos.filter(item => item.id === this.cart[i])[0];
    }
    this.ctrl = 1;
    console.log('cart', this.cart);
    console.log('nofilter', this.productos);
    console.log('filter', this.finalproductos);
    localStorage.setItem('badgecart', this.cart.length);
  }

  async filterProductos(){
    let loading = await this.loadingCtrl.create({
      message: 'Verificando la disponibilidad de tus productos',
    });
    loading.present().then(() => {
      let data = {
        "cart" : this.cart,
        "talla" : this.unittallacart,
        "numcart" : this.numcart,
        "unitcart": this.unitcart,
        "maduracioncart": this.maduracioncart,
        "preciocart" : this.preciocart,
      };
      
      this.server.ValidaInventario(data).then(d => {
        this.cart = d['cart'];
        this.unitcart = d['unitcart'];
        this.unittallacart = d['talla'];
        this.maduracioncart = d['maduracioncart'];
        this.numcart = d['numcart'];
        this.preciocart = d['precio'];

        localStorage.setItem('cart', JSON.stringify(this.cart));
        localStorage.setItem('numcart', JSON.stringify(this.numcart));
        localStorage.setItem('unitcart', JSON.stringify(this.unitcart));
        localStorage.setItem('unittallacart', JSON.stringify(this.unittallacart));
        localStorage.setItem('preciocart', JSON.stringify(this.preciocart));
        localStorage.setItem('maduracioncart', JSON.stringify(this.maduracioncart));

        this.Faltantes = d['mensaje'];
        this.finalproductos = [];
        for (let i = 0; i < this.cart.length; i++) {
          this.finalproductos[i] = this.productos.filter(item => item.id === this.cart[i])[0];
        }
        console.log('cart', this.cart);
        console.log('nofilter', this.productos);
        console.log('filter', this.finalproductos);
        localStorage.setItem('badgecart', this.cart.length);
        this.ctrl = 1;
        loading.dismiss();
        if(String(this.Faltantes).length>0){
          this.MuestraFaltantes();
        }
        this.calculateTotal();
      }).catch(error => {
        loading.dismiss();
        this.server.showAlert("mensaje", "<b>Ocurrio un problema, intenta nuevamente</b>");
        this.navCtrl.navigateForward("tabs/home");
      })
    });
  }

  filterProductostx() {
    
    this.finalproductos = [];
    let arr = [];

    this.auth.getGen('Inventario').then(data => {
      this.inventario = data;
      console.log(this.inventario);
      this.ctrl = 0;
      for (let i = 0; i < this.cart.length; i++) {
        this.Faltantes = "";
        if(!this.productos.filter(item => item.id === this.cart[i])[0]){
          console.log(this.cart[i][0]);
          this.removeCart(i);
          arr.splice(arr[i], 1);
        }
        else{
          arr[i] = this.productos.filter(item => item.id === this.cart[i])[0];
          console.log(arr[i]);
          let valor = this.inventario.find(itm => itm.id === parseInt(this.unitcart[i]));
          //console.log("Nombre---->",arr[i].nombreEs, "Precio---->",this.preciocart[i], "Inventario---->", valor)
          this.preciocart[i] = valor.precio*parseInt(this.numcart[i]);
          if(arr[i] === undefined || this.preciocart[i] === 0 || valor.talla !== this.unittallacart[i] || this.unittallacart[i] === 0 || valor.cantidad === 0 || !arr[i]['visible']){
            console.log(arr[i], this.preciocart[i], valor.talla +"!="+this.unittallacart[i],  this.unittallacart[i], valor.cantidad, arr[i]['visible']);
            this.Faltantes += "<p>• "+arr[i].nombreEs+"</p>";
            this.removeCart(i);
            arr.splice(arr[i], 1);
            this.ctrl = 1;
          }
        }
      }
      if(this.ctrl === 1)
      {
        console.log("entro a control");
        this.MuestraFaltantes();
        this.reloadCart();
      }
      this.finalproductos = arr;
      // console.log('cart', this.cart);
      // console.log('nofilter', this.productos);
      console.log('filter', this.finalproductos);
      localStorage.setItem('badgecart', this.cart.length);
      this.ctrl = 1;
      this.calculateTotal();
    })
  }

  filterProductosRemove() {
  	this.finalproductos = [];
    for (let i = 0; i < this.cart.length; i++) {
        this.finalproductos[i] = this.productos.filter(item => item.id === this.cart[i])[0];
    }
    console.log('cart', this.cart);
    console.log('nofilter', this.productos);
    console.log('filter', this.finalproductos);
    localStorage.setItem('badgecart', this.cart.length);
  }

  clearCart() {
  	this.finalproductos = [];
  	this.cart = []; 
  	this.numcart = [];
    this.unitcart = [];
    this.unittallacart = [];
    this.preciocart = [];
    this.maduracioncart = [];
  	localStorage.setItem('cart', JSON.stringify(this.cart));
    localStorage.setItem('numcart', JSON.stringify(this.numcart));
    localStorage.setItem('unitcart', JSON.stringify(this.unitcart));
    localStorage.setItem('unittallacart', JSON.stringify(this.unittallacart));
    localStorage.setItem('preciocart', JSON.stringify(this.preciocart));
    localStorage.setItem('maduracioncart', JSON.stringify(this.maduracioncart));
    this.filterProductos();
    this.calculateTotal();
    this.reloadCart();
  }

  continueCart() {
    let phoneNumber = JSON.parse(localStorage.user);
    phoneNumber = phoneNumber.telefono;
    this.Creditos(localStorage.uid);
    if(!phoneNumber){
      this.navCtrl.navigateForward('micuenta');
      this.server.showAlert('Mensaje', "Para continuar con la compra debes ingresar tu número telefónico");
      return;
    }
    console.log(this.subtotal);
    if(this.subtotal <= 0)
    {
      this.removeCart(null);
      this.server.showAlert('Carrito sin productos', "No tienes ningún producto en el carrito.");
      return;
    }
    if (this.cart.length > 0) {
      this.navCtrl.navigateForward('payload-program');
    } else {
      this.server.showAlert('Carrito sin productos', "No tienes ningún producto en el carrito.");
    }
  }

  reloadCart() {
  	this.navCtrl.navigateForward('tabs/cart');
  }

  openMenu() {
    this.menuCtrl.toggle();
  }

  gotoproduct(id: any) {
    localStorage.setItem('product-select', id);
    this.navCtrl.navigateForward('addproducto');
  }

  Creditos(uid){
    this.server.getCreditos(uid).then((data) => {
      if(data['ok']){
        console.log(data['link'].length);
        if(data['link'].length<=2)
          this.Link = "";
        else{
          this.Link = data['link'];
          console.log(data['link']);
          this.server.showAlert('Mensaje', 'Tienes un pago pendiente, por favor realízalo');
          this.navCtrl.navigateRoot('tabs/home')
        }
      }
      else{
        this.server.showAlert('Mensaje', 'No se pudieron obtener tus créditos');
        this.server.dismissLoading();
      }
    });
  }

  async MuestraFaltantes(){
    const alert = await this.alertctrl.create({
      header: 'Los siguentes productos ya no están disponibles',
      subHeader : 'Se han eliminado de tu carrito',
      message: '<b>'+this.Faltantes+'</b>',
      buttons: ['OK']
    });

    await alert.present();

  }

}
