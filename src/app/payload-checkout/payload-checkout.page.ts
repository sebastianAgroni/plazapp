import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { ServerService } from '../server/server.service';
import { UserService } from '../services/user-service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ToastController } from '@ionic/angular';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';


@Component({
  selector: 'app-payload-checkout',
  templateUrl: './payload-checkout.page.html',
  styleUrls: ['./payload-checkout.page.scss'],
})
export class PayloadCheckoutPage implements OnInit {
  range;
  method = 'Selecciona...';
  methodtipo;
  nombredia = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
  nombremethod = ['', 'Tarjeta Débito', 'Tarjeta Crédito', 'Datáfono', 'Contraentrega'];
  propina = '0';
  bolsa = '0';
  otherpropina = 'Otro';
  subtotal = '0';
  envio = '0';
  cuotas = '1';
  total;
  descuento = '0';
  valor_bolsa = '0';
  valor_bolsa_setting = 0;
  credito = localStorage.getItem('creditos');
  check_creditos: boolean = true;
  address;
  credDescont: any;
  Link;
  promo_master;
  porcDesc = 0;
  moneda = localStorage.moneda;
  referencia_divisa;
  valor_referencia;

  payload = {
    'metodoPago': '',
    'horarioRangoIni': 'dd/mm/aaaa hh:mm',
    'horarioRangoFin': 'dd/mm/aaaa hh:mm',
    'tarjetaId': '',
    'propina': '',
    'bolsa': '',
    'cvv': '',
    'rangoId': '0',
    'comentarios': '',
    'fechaSolicitud': 'dd/mm/aaaa hh:mm',
    'idDireccion': '',
    'uid': '',
    'total': '',
    'subtotal': '',
    'cupon_id': '',
    'cuotas': '1',
    'descuento' : '0',
    'productos' : [],
    'creditos' : '0'
    };
 
  sending: boolean = false;

  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController,
    private auth: AuthService,
    public server: ServerService,
    private user: UserService,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public iab : InAppBrowser,
    public toastController: ToastController,
    private firebaseAnalytics: FirebaseAnalytics) {
  }
  analitics(){
    this.firebaseAnalytics.logEvent('checkin', {page: "payload-checkout"})
  .then((res: any) => console.log(res))
  .catch((error: any) => console.error(error));
  }
  ngOnInit() {
    let precio = JSON.parse(localStorage.preciocart)
    let sum = 0;
    for(let p in precio){
      sum += parseInt(precio[p]);
    }
    localStorage.subtotal = sum;
  }

  async ionViewWillEnter() {
    this.analitics();
    const usu = this.auth.getUser();
    await this.ValidaSettings();
    const dir = parseInt(localStorage.getItem('address').split("~")[0]);
    this.address = dir;

    // Get Range Time
    const rang = localStorage.getItem('range-time');
    const rangsplit = rang.split('|');
    console.log('Horas', rangsplit);
    const dia = new Date(rangsplit[0]);
    this.range = this.nombredia[dia.getDay()] + ' ' + rangsplit[1] + ' a ' + rangsplit[2];
    this.range = rangsplit[0] + ' ' + rangsplit[1] + ' a ' + rangsplit[2];
    //this.credito = localStorage.getItem("creditos");
    
   this.descuento =  "" + (parseInt(localStorage.getItem('descuento')) + parseInt(this.credito));
   console.log(this.descuento, "acá calcula el descuento");

    // Get Method
    const methodactual = parseInt(localStorage.getItem('method'));
    if (!isNaN(methodactual)) {
      this.method = this.nombremethod[methodactual];
      this.methodtipo = "DEBITO";
      if (methodactual == 2) {
        console.log("Nombre Tarjeta",localStorage.getItem("tarjeta_nombre"));
        if (localStorage.getItem("tarjeta_nombre") === 'undefined' || localStorage.getItem("tarjeta_nombre") === null) {
          this.method = "Selecciona...";
          this.methodtipo = "";
          localStorage.setItem('method',"")
        } else {
          const tarjetanombre = localStorage.getItem('tarjeta_nombre').split('|');
          this.method = tarjetanombre[1]+' '+tarjetanombre[0];
          this.methodtipo = tarjetanombre[1];
        } 
      }
      if (methodactual == 4) {

      }
    }
    const capitalize = (s) => {
      if (typeof s !== 'string') return ''
      return s.charAt(0).toUpperCase() + s.slice(1)
    }
    this.method = capitalize(this.method.toLocaleLowerCase());

    // Get Propina
    const propinaactual = parseInt(localStorage.getItem('propina'));
    if (!isNaN(propinaactual)) {
       this.propina = String(propinaactual);
       if (propinaactual > 1000) {
         this.otherpropina = '$' + String(propinaactual);
         console.log("calculate propina");
         this.calculateTotal();
       }
    } else {
      this.propina = '1000';
      this.calculateTotal();
    }

    // Get Bolsa
    let tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Settings";
    this.auth.getGenKeyVal(tabla, 'llave', 'valor_bolsa').then(data => {
      let indata: any;
      indata = data;
      this.valor_bolsa_setting = indata[0].valor;
      const bolsaactual = parseInt(localStorage.getItem('bolsa'));

      if (!isNaN(bolsaactual)) {
        this.bolsa = String(bolsaactual);

        if (String(this.bolsa) == '0') {
          console.log('1');
          this.valor_bolsa = '0';
          console.log("calculate bolsa");
          this.calculateTotal();
        } else {
          console.log('2');
          this.valor_bolsa = String(this.valor_bolsa_setting);
          console.log("calculate bolsa else");
          this.calculateTotal();
        }
      }

    });
    this.credito = "0";

    //refencia a divisa
    await this.auth.getGenKeyVal(tabla,"llave","referencia_divisa").then(data => {
      this.referencia_divisa = data[0].estado;
      let valor : any = data[0].valor;
      let tasa = valor.tasa*this.total;
      this.valor_referencia = valor.texto+" "+tasa.toFixed(2);
    }).catch(error => {
      this.referencia_divisa = false;
    });

    await this.auth.getGenKeyVal(tabla,"llave","promo_master").then(data => {
      this.promo_master= data[0].valor;
    }).catch(error => {
      this.promo_master = {
        "estado" : false,
      }
    });

    //Validacion MasterCard
    if(this.promo_master.estado === true){
      let metodo = parseInt(localStorage.getItem('method'));
      if(parseInt(this.subtotal) >= parseInt(this.promo_master.monto) && metodo === 2){
        let id = localStorage.getItem('tarjeta_actual');
        if(parseInt(id) > 0){
          this.server.ValidaMasterPromo(id).then(async d => {
            if(d['ok']){
              let data = d['data'][0];
              await this.masterPromoAlert(data);
              this.porcDesc = parseFloat(data.Descuento) / 100;
              this.descuento = String((parseInt(this.subtotal)* this.porcDesc));
              localStorage.descuento = this.descuento;
              this.payload.cupon_id = data.codigo;
              localStorage.cupon = data.codigo;
              document.getElementById("CampoCupon").setAttribute("disabled","true");
              document.getElementById("buttonCupon").setAttribute("disabled","true");
              this.calculateTotal();
            }
          });
        }
      }
    }
    
    this.calculateTotal();
    this.Creditos(localStorage.uid);

  }

  async masterPromoAlert(data){
    const alert = await this.alertCtrl.create({
      header: "",
      cssClass : "CustomAlert",
      message: '<img src="'+data.Imagen+'" alt="g-maps" style="border-radius: 2px">',
      buttons: [
          {
              text: "cerrar",
              role: 'cancel',
          }
        ],
      });
    await alert.present();
  }

  calculateTotal() {
    // Set Subtotal, Envio, Total;
    let precio = JSON.parse(localStorage.preciocart)
    let sum = 0;
    for(let p in precio){
      sum += parseInt(precio[p]);
    }
    localStorage.subtotal = sum;
    this.subtotal = localStorage.getItem('subtotal');
    this.envio = localStorage.getItem('envio');
    console.log(parseInt(this.descuento), "Descuento en calculate total");
    if(isNaN(parseInt(this.descuento)))
      this.descuento = "0";
    this.total = parseInt(this.subtotal) + parseInt(this.envio) + parseInt(this.valor_bolsa) + parseInt(this.propina) - parseInt(this.descuento);
    //console.log('sub' + parseInt(this.subtotal) + 'envio' + parseInt(this.envio) + 'bolsa' + this.valor_bolsa + 'propina' + parseInt(this.propina) +"descuento"+ parseInt(this.descuento));
    //console.log(this.total);
  }

  setRange() {
    this.navCtrl.navigateBack('payload-program');
  }

  setMethod() {
    this.navCtrl.navigateForward('tipo-pago');
  }

  setPropina(value: string) {
    this.propina = value;
    const propinacheck = parseInt(value);
    if (propinacheck > 1000) {
      this.presentAlertPropina();
    } else {
      this.otherpropina = 'Otro';
      localStorage.setItem('propina', value);
    }
    this.calculateTotal();
  }

  setBolsa(value: string) {
    this.bolsa = value;
    localStorage.setItem('bolsa', value);
    if (value == '0') {
      this.valor_bolsa = '0';
    } else {
      this.valor_bolsa = String(this.valor_bolsa_setting);
    }
    this.calculateTotal();
  }

  ValidaCupon()
  {
    this.server.presentLoadingDefault('Validando tu cupón');
    let cupon = this.payload.cupon_id;
    localStorage.setItem("cupon", cupon);
    let user = this.user.getCurrentUser();
    this.user.getCurrentUser().then(user => {
      this.payload.uid = user.uid;
      this.payload.fechaSolicitud = new Date().toLocaleString('en-US');
      
      if(!cupon)
      {
        this.server.dismissLoading();
        this.server.showAlert('Error', 'Debes ingresar un cupón');
        return;
      }
      this.server.ValidaCodigo(this.payload).then((data) => {
        if (data['ok']) {
          this.server.dismissLoading();
          let arr = data['data'];
          this.user.getCurrentUser().then(user => {
            this.payload.uid = user.uid;
            this.payload.bolsa = this.bolsa;
            this.payload.propina = this.propina;
            this.payload.fechaSolicitud = new Date().toLocaleString('en-US');
            this.payload.idDireccion = localStorage.getItem('address');
            this.payload.tarjetaId = localStorage.getItem('tarjeta_actual');
            this.payload.subtotal = localStorage.getItem('subtotal');
            this.payload.total = this.total;
            this.payload.cvv = this.cuotas;
            this.payload.cuotas = this.cuotas;
            this.payload.metodoPago = localStorage.getItem('method');
          });
          if(parseInt(arr['valor']) > parseInt(this.subtotal))
          {
            this.server.dismissLoading();
            this.server.showAlert('Mensaje', 'Para redimir el cupón, tu compra debe ser mínimo de $'+arr['valor_format']);
            return;
          }
          let desc;
          if(this.credito > this.subtotal){
            desc = parseInt(this.subtotal) * (parseInt(arr['porcentaje'])/100);
            this.credDescont = this.credDescont - desc;
            console.log("entro a descontar el credito---> ", this.credDescont);
          }
          else{
            desc = parseInt(this.subtotal) * (parseInt(arr['porcentaje'])/100) + parseInt(this.credito);
            this.descuento = ""+desc;
            localStorage.setItem('descuento', this.descuento);
            let sub = parseInt(this.total) - (parseInt(this.subtotal) * (parseInt(arr['porcentaje'])/100));
            this.total = ""+sub
          }
          this.server.showAlert('Mensaje', 'Tu cupón se ha hecho efectivo!');
          document.getElementById("CampoCupon").setAttribute("disabled","true");
          document.getElementById("buttonCupon").setAttribute("disabled","true");
          this.navCtrl.navigateBack('payload-checkout');

        } else {
          this.payload.productos = [];
          this.server.showAlert('Mensaje', data['error_msg']);
          this.server.dismissLoading();
          this.sending = false;
          this.payload.cupon_id = "";
        }

      }).catch((err) => {
          this.payload.productos = [];
          this.server.showAlert('Error en cupón', 'Ha ocurrido un error al solicitar el cupón');
          this.server.dismissLoading();
          this.sending = false;
      });
    });
  }

  GuardaCupon()
  {
    // let cupon = this.payload.cupon_id;
    let cupon = localStorage.getItem("cupon");
    let user = this.user.getCurrentUser();
    this.user.getCurrentUser().then(user => {
      this.payload.uid = user.uid;
      this.payload.fechaSolicitud = new Date().toLocaleString('en-US');
      this.payload.creditos = localStorage.getItem("creditos");
      if(!cupon)
      {
        return;
      }
      this.server.GuardaCodigo(this.payload).then((data) => {
        if (data['ok']) {
          this.server.dismissLoading();
          let arr = data['data'];
          this.user.getCurrentUser().then(user => {
            this.payload.uid = user.uid;
            this.payload.bolsa = this.bolsa;
            this.payload.propina = this.propina;
            this.payload.fechaSolicitud = new Date().toLocaleString('en-US');
            this.payload.idDireccion = localStorage.getItem('address');
            this.payload.tarjetaId = localStorage.getItem('tarjeta_actual');
            this.payload.subtotal = localStorage.getItem('subtotal');
            this.payload.total = this.total;
            this.payload.cvv = this.cuotas;
            this.payload.cuotas = this.cuotas;
            this.payload.metodoPago = localStorage.getItem('method');
            this.payload.creditos = localStorage.getItem("creditos");
          });
        } else {
          this.sending = false;
        }

      }).catch((err) => {
          this.payload.productos = [];
          this.server.showAlert('Error en guardar cupón', 'Ha ocurrido un error al solicitar el cupón');
          this.server.dismissLoading();
          this.sending = false;
      });
    });
  }

  valcheckout()
  {
    // let tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Settings";
    // this.auth.getGenKeyVal(tabla, "llave", "valor_minimo").then(function (data) {
    //   var indata;
    //   indata = data;
    //   console.log("settings",indata);
    // });
    let address;
    if(localStorage.address){
      address = localStorage.address.split("~")[1];
    }
    else{
      address = '0';
    }
    
    if(address === '0')
    {
      this.server.showAlert('Mensaje','Por favor selecciona una dirección de entrega');
      return;
    }
    // this.user.getCurrentUser().then(user => {
    this.sending = true;

    let datos = {
      "uid" : localStorage.uid,
      "direccion" : address,
    }

    console.log(datos, "datos --->");

    this.server.valiAddressPay(JSON.stringify(datos)).then((data)=>{
      //this.server.presentLoadingDefault('Validando dirección');
      console.log("---->", data);
      let resp: any = data;
      if(resp.ok)
      {
        this.checkout();
      }
      else
      {
        //this.server.dismissLoading();
        this.server.showAlert('Mensaje',resp.error_msg);
        this.sending = false;
        console.log('No esta dentro de poligono');
        return;
      }
    }).catch(error => {
      console.log("error", error);
    });
    //});
  }

  // checkout() {
  //   this.navCtrl.navigateRoot('payload-done');
  // }

  async checkout() {

    let loading = await this.loadingCtrl.create();

    loading.present().then(() => {
      this.envio = localStorage.getItem('envio');

      if(this.check_creditos)    {
        localStorage.setItem("creditos", this.credDescont)
      }
      else{
        localStorage.setItem("creditos", "0");
      }

      this.server.dismissLoading();
      this.sending = true;
      this.server.presentLoadingDefault('Realizando Pedido');
      this.calculateTotal();

      //Validación que el pedido no se vaya en 0
      if(parseInt(this.subtotal) > parseInt(this.credito)){
        if(isNaN(this.total) || this.total == 0 || this.total == 'NaN' || this.total == '0'){
          this.sending = true;
          this.server.dismissLoading();
          this.server.showAlert('Error de Pedido', 'Ha ocurrido un error al solicitar el pedido, por favor intentalo nuevamente');
          this.calculateTotal();
          this.sending = false;
          return;
        }
      }

      //this.user.getCurrentUser().then(user => {
        this.payload.uid = localStorage.uid;
        this.payload.bolsa = this.bolsa;
        this.payload.propina = this.propina;
        this.payload.fechaSolicitud = new Date().toLocaleString('en-US');
        this.payload.idDireccion = localStorage.getItem('address');
        this.payload.tarjetaId = localStorage.getItem('tarjeta_actual');
        this.payload.subtotal = localStorage.getItem('subtotal');
        this.payload.total = this.total;
        this.payload.cvv = this.cuotas;
        this.payload.cuotas = this.cuotas;
        this.payload.metodoPago = localStorage.getItem('method');
        this.payload.descuento = this.descuento;

        const rang = localStorage.getItem('range-time');
        const rangsplit = rang.split('|');
        console.log('Horas', rangsplit);
        const ini = rangsplit[0] + ' ' + rangsplit[1];
        const fini = rangsplit[0] + ' ' + rangsplit[2];

        let fechadia = new Date(rangsplit[0]);
        let dia = fechadia.getFullYear()+"-"+fechadia.getMonth()+"-"+fechadia.getDay();

        console.log("dia",dia);

        this.payload.horarioRangoIni = ini;
        this.payload.horarioRangoFin = fini;


        const cart = JSON.parse(localStorage.getItem('cart'));
        const numcart = JSON.parse(localStorage.getItem('numcart'));
        const unitcarttalla = JSON.parse(localStorage.getItem('unittallacart'));
        const preciocart = JSON.parse(localStorage.getItem('preciocart'));
        const maduracioncart = JSON.parse(localStorage.getItem('maduracioncart'));


        for (let i = 0; i < cart.length; i++) {
          const productocart = {};
          productocart['id'] = cart[i];
          productocart['numcart'] = numcart[i];
          productocart['unitcart'] = unitcarttalla[i];
          productocart['precio'] = preciocart[i];
          if (maduracioncart[i] == 'Verde') {
            productocart['maduracion_id'] = '1';
          } else if (maduracioncart[i] == 'Maduro') {
            productocart['maduracion_id'] = '3';
          } else {
            productocart['maduracion_id'] = '2';
          }
          this.payload.productos.push(productocart);
        }

        console.log('Log Pedido', this.payload);

        //Pago Contraentrega Temporal
        if(this.payload.metodoPago === "4"){
          let address;
          if(localStorage.address){
            address = localStorage.address.split("~")[1];
          }
          else{
            address = '0';
          }
          this.payload.idDireccion = address;
          this.server.addPedidoPg(JSON.stringify(this.payload)).then((data) => {
            if (data['status']) {
              this.GuardaCupon();
              this.saveCreditos();
              this.server.dismissLoading();
              loading.dismiss();
              this.sending = true;
              this.payload.productos = [];
              this.navCtrl.navigateForward('payload-done');
            } else {
              this.payload.productos = [];
              this.server.showAlert('Error de Pedido', data['respuesta']);
              this.server.dismissLoading();
              loading.dismiss();
              this.sending = false;
            }
          }).catch((err) => {
            this.payload.productos = [];
            this.server.showAlert('Error de Pedido', 'Ha ocurrido un error al solicitar el pedido');
            this.server.dismissLoading();
            loading.dismiss();
            this.sending = false;
          });
        }
        else{
          this.server.addPedido(this.payload).then((data) => {
            if (data['ok']) {
              this.GuardaCupon();
              this.saveCreditos();
              this.server.dismissLoading();
              loading.dismiss();
              this.sending = true;
              this.payload.productos = [];
              // console.log("method ---->",localStorage.getItem('method'));
              // if(localStorage.getItem('method') == "1"){
              //   this.PagoEnLinea();
              //   return;
              // }
              this.navCtrl.navigateForward('payload-done');
            } else {
              this.payload.productos = [];
              this.server.showAlert('Error de Pedido', data['error_msg']);
              this.server.dismissLoading();
              loading.dismiss();
              this.sending = false;
            }
          }).catch((err) => {
            this.payload.productos = [];
            this.server.showAlert('Error de Pedido', 'Ha ocurrido un error al solicitar el pedido');
            this.server.dismissLoading();
            loading.dismiss();
            this.sending = false;
          });
        }

        
      //});
    });
  }

  checkTarjeta() {
    const methodactual = parseInt(localStorage.getItem('method'));
    if (!isNaN(methodactual)) {
      if (methodactual == 2) { return false ; } else { return true; }
    }
  }

  async goBack() {
    if(parseInt(localStorage.descuento)> 0){
      localStorage.setItem("descuento", "0");
      localStorage.removeItem("cupon");
      document.getElementById("CampoCupon").setAttribute("disabled","false");
      document.getElementById("buttonCupon").setAttribute("disabled","false");
      this.payload.cupon_id = "";
      const toast = await this.toastController.create({
        message: 'Se ha anulado tu descuento. Si utilizaste un cupón asegurate de ingresarlo nuevamente!',
        duration: 5000
      });
      toast.present();
    }
    //this.navCtrl.pop();
    this.navCtrl.navigateForward('tipo-pago');
  }

  async presentAlertPropina() {
    const alert = await this.alertCtrl.create({
      header: 'Ingresa tu propina',
      inputs: [
      {
        name: 'otherpropina',
        placeholder: '$0'
      }
    ],
      buttons: [
      {
        text: 'Cancelar',
        role: 'cancelar',
        handler: data => {
          console.log('Cancel clicked');
          this.propina = '1000';
          localStorage.setItem('propina', '1000');
          this.calculateTotal();
        }
      },
      {
        text: 'Guardar',
        handler: data => {
          this.propina = data.otherpropina;
          this.otherpropina = '$' + data.otherpropina;
          localStorage.setItem('propina', data.otherpropina);
          this.calculateTotal();
        }
      }
      ]
    });

    await alert.present();
  }

  ValidaSettings(){
    var subtotal = this.subtotal = localStorage.getItem('subtotal');
    this.server.getSettings(subtotal).then((data) => {
       if(data['ok']){
          var datos = data['datos'];
          for(var i in datos){
            if(datos[i]['llave'] === 'valor_minimo')
            {
              var pedidoMinimo = datos[i]['valor'];
            }
          }
          if(parseInt(subtotal) < parseInt(pedidoMinimo))
          {
            this.server.showAlert('Mensaje', 'Tu pedido deber ser mínimo de $'+pedidoMinimo);
            this.navCtrl.navigateBack('tabs/home');
          }
       }
    });
    //console.log("subtotal", subtotal);
    //this.server.showAlert('Mensaje', 'Tu pedido deber ser minimo de $');
    //this.navCtrl.navigateBack('tabs/home');
    return false;
  }

  addValue(e): void {
    var checked = e.currentTarget.checked
    if(checked == true){
      this.check_creditos = true;
      this.descuento =  "" + (parseInt(this.descuento) + parseInt(this.credDescont));
      if( document.getElementById("TotDesc"))
        document.getElementById("TotDesc").setAttribute("style","display:block");
    }
    else{
      this.check_creditos = false;
      this.descuento =  "" + (parseInt(this.descuento) - parseInt(this.credDescont));
      if( document.getElementById("TotDesc"))
        document.getElementById("TotDesc").setAttribute("style","display:none");
    }
    this.calculateTotal();
  }

  saveCreditos(){
    this.user.getCurrentUser()
    .then(user => {
      const uid = user.uid;
      this.credito = localStorage.getItem("creditos");
      this.server.saveCreditos(uid, this.credito, this.envio).then((data) => {});
    });
  }

  Creditos(uid){
    this.server.getCreditos(uid).then((data) => {
      if(data['ok']){
        this.credito = data['res'][0].valor;
        console.log("Fruticoins : "+this.credito);
        if(this.credito > this.subtotal){
          this.descuento = this.subtotal;
          this.credDescont = this.subtotal;
        }
        else{
          this.descuento = String(parseInt(this.credito) + parseInt(localStorage.getItem('descuento')));
          this.credDescont = this.credito;
        }
        localStorage.setItem("creditos", this.credito);
        console.log("calculate total creditos");
        this.calculateTotal();
      }
      else{
        this.server.showAlert('Mensaje', 'No se pudieron obtener tus créditos');
        this.server.dismissLoading();
      }
    });
  }

  async modalFruitcoins(){
    const alertct = await this.alertCtrl.create({
      header: 'FrutiCoins',
      message: '<div class="cupon-text">Los <b>FrutiCoins</b> son dinero para pagar tus mercados en Plaz.<br>Puedes seleccionarlos para usarlos en esta compra, o desactivarlos para una futura compra. </div>',
      buttons: ['OK']
    });
    await alertct.present();
  }

  PagoEnLinea(){
    /*clean*/
    let cart = [];
    let numcart = [];
    let unitcart = [];
    let badgecart = "0";
    let preciocart = [];
    let maduracioncart = [];
    localStorage.setItem("cart",JSON.stringify(cart));
    localStorage.setItem("numcart",JSON.stringify(numcart));
    localStorage.setItem("unitcart",JSON.stringify(unitcart));
    localStorage.setItem("badgecart",badgecart);
    localStorage.setItem("preciocart",JSON.stringify(preciocart));
    localStorage.setItem("maduracioncart",JSON.stringify(maduracioncart));

    this.server.getCreditos(localStorage.uid).then((data) => {
      if(data['ok']){
        console.log(data['link'].length);
        console.log(data['link']);
        if(data['link'].length<=1)
          this.Link = "";
        else{
          this.Link = data['link'];
          let browser = this.iab.create(
            this.Link,
            "_blank",
            'location=yes,hidden=no,clearcache=yes,clearsessioncache=yes,zoom=yes,hardwareback=yes,mediaPlaybackRequiresUserAction=no,shouldPauseOnSuspend=no, closebuttoncaption=Close,disallowoverscroll=no,toolbar=yes,enableViewportScale=no,allowInlineMediaPlayback=no,presentationstyle=pagesheet,fullscreen=yes,',
            
          );
          browser.on('loadstart').subscribe( event => {
          var closeUrl = 'https://huerto.plaz.com.co/tabs/home'; 
          if(event.url == closeUrl)
            {
              browser.close();
              this.navCtrl.navigateForward('tabs/home')
            }
          });
        } 
      }
      else{
        this.navCtrl.navigateRoot('tabs/home')
        this.server.dismissLoading();
      }
    });
  }
}