import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AngularFireModule } from 'angularfire2';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { firebaseConfig } from '../config';

import { AuthService } from './services/auth.service'; 
import { ParametrosService } from './services/parametros.service'; 

import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';

import { FormsModule } from '@angular/forms';

import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';

import { TabsPageModule } from './tabs/tabs.module'; 

import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';

import { ServerService } from "./server/server.service";
import { UserService } from './services/user-service';

import {UniqueDeviceID} from "@ionic-native/unique-device-id/ngx";

import { HTTP } from '@ionic-native/http/ngx';

import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

import { OneSignal } from '@ionic-native/onesignal/ngx';


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,
  	AngularFireModule.initializeApp(firebaseConfig.fire),
    AngularFireDatabaseModule,FormsModule,TabsPageModule, HttpClientModule],
  providers: [
    CallNumber,
    Keyboard,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    AngularFireAuth,
    AngularFireDatabase,
    AuthService,
    ScreenOrientation,
    FirebaseAuthentication,
    ParametrosService,
    Facebook,
    GooglePlus,
    HttpClient,
    ServerService,
    UserService,
    UniqueDeviceID,
    InAppBrowser,
    HTTP,
    OneSignal
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

