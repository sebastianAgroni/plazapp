import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController, LoadingController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AuthService } from './services/auth.service';
import { IntroPage } from './intro/intro.page';
import { HomePage } from './home/home.page';
import { EventEmitterService } from './services/events/event-emitter.service';
import { Subscription } from 'rxjs';
import { PushService } from "./services/push.service";
import { ServerService } from './server/server.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {

  activePage: any;
  user;
  items: Array<{title: string, Component: any}>;
  nologin: boolean;
  listener: Subscription;
  public loading = null;
  uid : any;
  valorCredito;
  code = localStorage.uid == true ? localStorage.uid.substr(5,8) : "";

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private auth: AuthService,
    public nav: NavController,
    public menuCtrl: MenuController,
    public event: EventEmitterService,
    public loadingCtrl: LoadingController,
    private pushService : PushService,
    public server : ServerService
  ) {
    this.initializeApp();
  }

  async createLoading() {
    this.loading = await this.loadingCtrl.create({
      spinner: "bubbles",
      message: "Cargando...",
      translucent: false,
      keyboardClose: true,
      duration: 8000
    });
    return await this.loading.present();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.pushService.ConfigPush();
      console.log('App load sucessfull');
      // if (this.platform.is('cordova') ) {
        this.statusBar.styleDefault();
        this.splashScreen.hide();
      // }
      // this.platform.backButton.subscribe(() => {
      //   this.nav.pop();
      // });

      this.listener = this.event.eventEmitter.subscribe(state => {
        switch (state) {
          case true:
            console.log("==> true");
            if(this.loading === null) this.createLoading();
            break;
          case false:
            if(this.loading !== null) {
              console.log("==> DISMISS");
              this.loading.dismiss();
              this.loading = null;
            }
            break;
        
          default:
            break;
        }
      });
  
      this.event.sendEmit();
    });

    // Menu Data Builder
     const itemslogin = [
      {
        title: '<p>Home</p>', 
        Component: 'tabs/home', 
        icon : "https://firebasestorage.googleapis.com/v0/b/plazapp-test/o/home.svg?alt=media&token=473692f5-db37-46b3-909f-00fdb2882ec0", 
        class : "home"
      },
      {
        title: '<p>Mi Cuenta<p>', 
        Component: 'micuenta',
        icon : "https://firebasestorage.googleapis.com/v0/b/plazapp-test/o/account.svg?alt=media&token=111d1ec1-9a3f-4977-98de-b4181b43d62b",
        class : ""
      },
      {
        title: '<p>Historial de Pedidos<p>', 
        Component: 'historial',
        icon : "https://firebasestorage.googleapis.com/v0/b/plazapp-test/o/commerce-and-shopping.svg?alt=media&token=dbc754f8-e54a-4a39-b777-02115d7d8d75",
        class : ""
      },
      {
        title: '<p>Mis Direcciones<p>', 
        Component: 'mis-direcciones',
        icon : 'https://firebasestorage.googleapis.com/v0/b/plazapp-test/o/signs.svg?alt=media&token=e88d4582-2032-40f5-bd74-4036807103b4',
        class : ''
      },
      {
        title: '<p>Blog</p>', 
        Component: 'blog',
        icon : 'https://firebasestorage.googleapis.com/v0/b/plazapp-test/o/square.svg?alt=media&token=42cca8ab-dd37-46ae-9eaf-78502e9b836b',
        class : '',
      },
      {
        title: '<p>Fruticoins</p>', 
        Component: 'mis-creditos',
        icon : 'https://firebasestorage.googleapis.com/v0/b/plazapp-test/o/fruticoins-white.svg?alt=media&token=d9250d15-faf0-4936-8c77-f5f15a2022fd',
        class : 'fruticoins',
      },
      // {
      //   title: '<p>Cód. Referidos</p><small>¡Comparte tu código y gana fruticoins!</small>', 
      //   Component: 'mis-creditos',
      //   icon : 'https://firebasestorage.googleapis.com/v0/b/plazapp-test/o/communications.svg?alt=media&token=c8563337-3f23-479c-b658-24e694c945e5',
      //   class : 'CodReferido',
      // },
      {
        title: '<p>Cerrar Sesión</p>',
        Component: null,
        icon : 'https://firebasestorage.googleapis.com/v0/b/plazapp-test/o/logout.svg?alt=media&token=5b3ca3b8-d09f-4d6b-b5eb-5c1ae19d2adc',
        class : ''
      }
    ];

    const itemsnologin = [
      {title: 'Home', Component: 'home'},
      {title: 'Blog', Component: 'blog'},
      {title: 'Ingresar', Component: 'log'},
      {title: 'Registrar', Component: 'registro'}
    ];
    // Detect User Auth Login
    // this.auth.afAuth.authState
    // .subscribe(
    //     user => {
    //       if (user) {
    //         this.user = this.auth.getUser();
    //         this.nav.navigateRoot('tabs/home');
    //         this.items = itemslogin;
    //         this.Creditos(user.uid);
    //         if(!localStorage.getItem('uid')){
    //           this.code = ""; 
    //           localStorage.uid = "";
    //         }
    //         else{
    //           var uid = localStorage.uid;
    //           this.code = uid.substr(5,8);
    //         }
    //       } else {
    //         this.nav.navigateRoot('tabs/home');
    //         this.nologin = true;
    //         this.items = itemsnologin;
    //       }
    //     },
    //     () => {
    //       this.user = null;
    //       this.nav.navigateRoot('intro');
    //     } 
    //   );
    var uid = localStorage.uid;
    var user = localStorage.user

    if(typeof user === 'undefined'){
      localStorage.clear();
      this.items = itemslogin;
      this.nav.navigateRoot('intro');
    }

    if(!uid){
      this.items = itemslogin;
      this.nav.navigateRoot('intro');
    }
    else{
      try {
        this.user = JSON.parse(localStorage.user);
        this.items = itemslogin;
        this.nav.navigateRoot('tabs/home');
      } catch (error) {
        localStorage.clear();
        this.items = itemslogin;
        this.nav.navigateRoot('intro');
      }
     
    }
  }

  openPage(page) {
    if (page) {
      this.nav.navigateRoot(page);
      this.menuCtrl.toggle();
    } else {
        //this.auth.signOut();
        localStorage.clear();
        this.nav.navigateRoot('intro');
    }
    this.activePage = page;
  }

  public checkActivePage(page): boolean {
    return page === this.activePage;
  }

  Creditos(uid){
    this.server.getCreditos(uid).then((data) => {
      if(data['ok']){
        this.valorCredito = data['res'][0].valor;
        localStorage.setItem("creditos", this.valorCredito);
      }
      else{
        this.server.showAlert('Mensaje', 'No se pudieron obtener tus créditos');
        this.server.dismissLoading();
      }
    });
  }

  }
