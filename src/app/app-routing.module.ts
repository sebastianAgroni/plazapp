import { NgModule } from '@angular/core';
import { PreloadAllModules, Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  //{ path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'ayuda', loadChildren: './ayuda/ayuda.module#AyudaPageModule' },
  { path: 'bienvenido', loadChildren: './bienvenido/bienvenido.module#BienvenidoPageModule' },
  { path: 'intro', loadChildren: './intro/intro.module#IntroPageModule' },
  { path: 'log', loadChildren: './log/log.module#LogPageModule' },
  { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'registro', loadChildren: './registro/registro.module#RegistroPageModule' },
  { path: 'repass', loadChildren: './repass/repass.module#RepassPageModule' },
  { path: 'cart', loadChildren: './cart/cart.module#CartPageModule' },
  { path: 'terminos', loadChildren: './terminos/terminos.module#TerminosPageModule' },
  { path: 'buscar', loadChildren: './buscar/buscar.module#BuscarPageModule' },
  { path: 'shop', loadChildren: './shop/shop.module#ShopPageModule' },
  { path: 'contacto', loadChildren: './contacto/contacto.module#CONTACTOPageModule' },
  { path: 'addproducto', loadChildren: './addproducto/addproducto.module#AddproductoPageModule' },
  { path: 'micuenta', loadChildren: './micuenta/micuenta.module#MicuentaPageModule' },
  { path: 'recetas', loadChildren: './recetas/recetas.module#RecetasPageModule' },
  { path: 'receta', loadChildren: './receta/receta.module#RecetaPageModule' },
  { path: 'propiedades', loadChildren: './propiedades/propiedades.module#PropiedadesPageModule' },
  { path: 'historial', loadChildren: './historial/historial.module#HistorialPageModule' },
  { path: 'blog', loadChildren: './blog/blog.module#BlogPageModule' },
  { path: 'entrada-blog', loadChildren: './entrada-blog/entrada-blog.module#EntradaBlogPageModule' },
  { path: 'tarjetas', loadChildren: './tarjetas/tarjetas.module#TarjetasPageModule' },
  { path: 'addtarjeta', loadChildren: './addtarjeta/addtarjeta.module#AddtarjetaPageModule' },
  { path: 'payload-program', loadChildren: './payload-program/payload-program.module#PayloadProgramPageModule' },
  { path: 'payload-checkout', loadChildren: './payload-checkout/payload-checkout.module#PayloadCheckoutPageModule' },
  { path: 'tipo-pago', loadChildren: './tipo-pago/tipo-pago.module#TipoPagoPageModule' },
  { path: 'mapa', loadChildren: './mapa/mapa.module#MapaPageModule' },
  { path: 'payload-done', loadChildren: './payload-done/payload-done.module#PayloadDonePageModule' },
  { path: 'chat', loadChildren: './chat/chat.module#ChatPageModule' },
  { path: 'ver-compra', loadChildren: './ver-compra/ver-compra.module#VerCompraPageModule' },
  { path: 'mis-direcciones', loadChildren: './mis-direcciones/mis-direcciones.module#MisDireccionesPageModule' },
  { path: 'mis-creditos', loadChildren: './mis-creditos/mis-creditos.module#MisCreditosPageModule' },
  { path: 'add-adress-geo', loadChildren: './add-adress-geo/add-adress-geo.module#AddAdressGeoPageModule' },
  { path: 'payload-owndata', loadChildren: './payload-owndata/payload-owndata.module#PayloadOwndataPageModule' },
  { path: 'verify-tel', loadChildren: './verify-tel/verify-tel.module#VerifyTelPageModule' },

  //{ path: 'form-payu', loadChildren: './form-payu/form-payu.module#FormPayuPageModule' },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
