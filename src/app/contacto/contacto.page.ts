import { Component, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.page.html',
  styleUrls: ['./contacto.page.scss'],
})
export class CONTACTOPage implements OnInit {

  chat_enable = "1";
  whatsapp = "Sin Whatsapp";
  email = "Sin Email";
  telefono = "Sin Télefono";

  constructor(private callNumber: CallNumber,
    public menuCtrl: MenuController, 
    private nav: NavController,
    private auth: AuthService) {}

  ngOnInit() {
  }

  ionViewWillEnter(){
    const ciudad = localStorage.ciudad;
    //Get setting for contact

    let tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Settings";
    console.log(tabla);
    this.auth.getGenKeyVal(tabla,"llave","chat_habilitado").then(data => {
      let indata: any;
      indata = data;
      this.chat_enable = indata[0].valor;
      console.log("Chat ",this.chat_enable);
    });
    this.auth.getGenKeyVal(tabla,"llave","whatsapp_contacto").then(data => {
      let indata: any;
      indata = data;
      this.whatsapp = indata[0].valor;
      console.log("Whatsapp ",this.whatsapp);
    });
    this.auth.getGenKeyVal(tabla,"llave","email_contacto").then(data => {
      let indata: any;
      indata = data;
      this.email = indata[0].valor;
      console.log("Email ",this.email);
    });
    this.auth.getGenKeyVal(tabla,"llave","telefono_contacto").then(data => {
      let indata: any;
      indata = data;
      this.telefono = indata[0].valor;
      console.log("Telefono ",this.telefono);
    });
  }

  openPhone(phone: string){
  	this.callNumber.callNumber(phone, true)
	  .then(res => console.log('Launched dialer!', res))
	  .catch(err => console.log('Error launching dialer', err));
  }

  openWhatsapp(tel: String){
    // if(localStorage.ciudad > 1)
    //   window.open("https://api.whatsapp.com/send?phone="+tel+"&text=&source=&data=&app_absent=",'_system');
    // else
    //   window.open("https://bit.ly/PlazWhatsapp",'_system')
    window.open("https://api.whatsapp.com/send?phone="+tel+"&text=&source=&data=&app_absent=",'_system');
  }

  openEmail(){
  	window.open('mailto:'+this.email, '_system');
  }

  openChat(){
    this.nav.navigateForward("chat");
  }

  openMenu() {
    this.menuCtrl.toggle();
  }

}
