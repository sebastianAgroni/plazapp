import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController, IonSelect, Events } from '@ionic/angular';

import { AuthService } from '../../services/auth.service';
import { ServerService } from '../../server/server.service';
import { UserService } from '../../services/user-service';


@Component({
  selector: 'app-direcciones',
  templateUrl: './direcciones.component.html',
  styleUrls: ['./direcciones.component.scss'],
})
export class DireccionesComponent implements OnInit {
  @ViewChild('dirs', { static: true }) select: IonSelect;

	address: any = '0';
  direccion = {
    tienda: '',
    dir: '',
    adicional: '',
    ciudad: {
      nombre: 'Bogotá D.C',
      id: '1'
    },
    predeterminada: 0,
    lat: 0,
    lng: 0,
    uid: '',
    id: 0
  };
  direcciones: any;
  validaStatus;

  constructor(public alertCtrl: AlertController,
    private auth: AuthService,
    public menuCtrl: MenuController,
    public server: ServerService,
    public nav: NavController,
    public userService: UserService,
    public navCtrl: NavController,
    public events: Events) {
    events.subscribe('user:open', () => {
      this.presentAlertAddress();
    });
    this.userService.getCurrentUser()
    .then(user => {
      this.auth.getGenKeyVal('Envio','uid', user.uid).then(data => {
        this.direcciones = data;
        localStorage.setItem('list-address', JSON.stringify(data));
        console.log('Direcciones: ', data);
      });
    });
    const pre_address = localStorage.getItem('address');
    if (pre_address != null) {
      this.address = pre_address;
    }
  }

  ngOnInit() {
    this.checkAddress(null);
  }

  checkAddress(event: any) {
    if (this.address =='-1') {
      //this.presentAlertAddress();
      this.nav.navigateForward('add-adress-geo');
    } else {
      localStorage.setItem('address', this.address);
    }
  }

  isValid (dir: string, city : string, other: string) {

    if (dir.length <= 4) {
      return false;
    }
    let direccion = dir;
    let direc  = dir +", "+city;
    this.userService.getCurrentUser().then(user => {
      this.server.getGeocode(user.uid).then((data)=>{
      let resp: any = data;
      if(resp.ok){
        this.direccion.dir = direccion;
        this.direccion.adicional =  other;
        const user = this.auth.getUser();
        this.direccion.uid = user.uid;
        this.server.presentLoadingDefault('Agregando Dirección');
        this.server.addDireccion(this.direccion).then((data) => {
          this.server.dismissLoading();
          this.server.showAlert('Dirección Agregada','Dirección Agregada con éxito');
          this.nav.navigateForward('mis-direcciones');
        });
        return;
      } else{
        console.log('no added address');
        this.server.showAlert('Mensaje',resp.error_msg);
        this.nav.navigateForward('mis-direcciones');
        return;
      }
      }).catch((err)=>{
          this.validaStatus = false;
          this.server.showAlert('Error Red',"No se pudo conectar con el servidor.");
      });
    });
  }

  getdata() {
    console.log('ok');
  }

  async presentAlertAddress() {
    const alert = await this.alertCtrl.create({
      header: 'Añadir Dirección',
      message: 'Ciudad: <select class="select-city" id="city"><option value="1">Bogotá</option></select>',
      inputs: [
      {
        name: 'direccion',
        placeholder: 'Dirección de Entrega'
      },
      {
        name: 'more',
        placeholder: 'Información Adicional'
      }
    ],
      buttons: [
      {
        text: 'Cancelar',
        role: 'cancelar',
        handler: data => {
          console.log('Cancel clicked');
          this.address = '0';
        }
      },
      {
        text: 'Verificar',
        handler: data => {
          const city = (<HTMLInputElement>document.getElementById('city')).value;
          this.isValid(data.direccion, city, data.more);
          // if (vali) {
          //   this.direccion.dir = data.direccion;
          //   this.direccion.adicional =  data.more;
          //   const user = this.auth.getUser();
          //   this.direccion.uid = user.uid;
          //   this.server.presentLoadingDefault('Agregando Dirección');
          //   this.server.addDireccion(this.direccion).then((data) => {
          //     if (data['ok']) {
          //       this.server.dismissLoading();
          //       this.server.showAlert('Dirección Agregada','Dirección Agregada con éxito');
          //       this.nav.navigateForward('mis-direcciones');
          //     } else {
          //       this.server.dismissLoading();
          //       this.server.showAlert('Error Dirección', data.error_msg);
          //     }
          //   });
          // } else {
          //   //this.server.showAlert('Mensaje',data.error_msg);
          //   console.log('no added address');
          //   return false;
          // }
        }
      }
      ]
    });

    await alert.present();
  }

}
