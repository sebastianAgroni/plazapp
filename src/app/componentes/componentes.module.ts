import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from "@angular/forms";

import { DireccionesComponent } from './direcciones/direcciones.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule.forRoot(),
  ],
  declarations: [
  	DireccionesComponent
  ],
  exports: [
  	DireccionesComponent
  ],
  entryComponents: [],
})
export class ComponentsModule {}