import { Component } from '@angular/core';

import { HomePage } from '../home/home.page';

import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})

export class TabsPage {    
  cart: any = "0";

  constructor(public alertController: AlertController) {
    this.updateBadge();
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Nota',
      message: 'Se abrira tu aplicacion de correo',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Ok',
          handler: () => {
            window.open(`mailto:natalia@listapp.com.co`, '_system');
          }
        }
      ]
    });

    await alert.present();
  }

  updateBadge(){
    let badgecart = localStorage.getItem('badgecart');
    this.cart = badgecart;
  }

  ionViewWillEnter(){
    this.updateBadge();
  }

}

