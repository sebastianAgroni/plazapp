import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { ParametrosService } from '../services/parametros.service';


@Component({
  selector: 'app-entrada-blog',
  templateUrl: './entrada-blog.page.html',
  styleUrls: ['./entrada-blog.page.scss'],
})
export class EntradaBlogPage implements OnInit {

	post: any;
  postid: any;

  constructor(public menuCtrl: MenuController, public navCtrl: NavController,public param: ParametrosService) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.param.serviceData
    .subscribe(post => (this.postid = post));
    console.log("Recibiendo postid: ", this.postid);
    this.post = JSON.parse(localStorage.getItem('blog'));
    this.post = this.post.filter(item => item.id === this.postid)[0];
  }

  goBack() {
    this.navCtrl.pop();
  }
}
