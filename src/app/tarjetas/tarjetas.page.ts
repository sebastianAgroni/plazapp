import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { ServerService } from "../server/server.service";
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user-service';

@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.page.html',
  styleUrls: ['./tarjetas.page.scss'],
})
export class TarjetasPage implements OnInit {
  fromCartNav = false;
	tarjetas: any;
  datos = {
    "uid": "",
    "id": ""
  };
  constructor(public navCtrl: NavController, 
    private auth: AuthService, 
    public server: ServerService, 
    private user: UserService) { }

  ngOnInit() {
    let fromCart = localStorage.getItem("fromCart");
    if (fromCart=="true") {
      this.fromCartNav = true;
    }
    this.getTarjeta();
  }


  goBack() {
    this.navCtrl.navigateBack("micuenta");
  }

  goBackT() {
    this.navCtrl.navigateBack("tipo-pago");
  }

  goAddTarjeta() {
    this.navCtrl.navigateForward("addtarjeta");
  }

  Selection(item: any) {
	   this.tarjetas.forEach(x => { x.checked = false });
     localStorage.setItem("tarjeta_actual",item.id);
     localStorage.setItem("tarjeta_nombre",item.ultimos+"|"+item.tipoTarjeta);
     let fromCart = localStorage.getItem("fromCart");
     if (fromCart=="true") {
        //this.navCtrl.navigateBack("payload-checkout");
        this.navCtrl.navigateForward("tipo-pago");
    }
	}

  getTarjeta() {
    let uid = localStorage.uid
    // this.auth.getGen('Tarjeta/'+uid).then(data => {
    //   this.tarjetas = data;    
    //   let actualtarjeta = localStorage.getItem("tarjeta_actual") || 0;
    //   console.log("Tarjeta Actual",actualtarjeta);
    //   for (var i = 0; i < this.tarjetas.length; i++) {
    //     if(this.tarjetas[i].id==actualtarjeta) {
    //       this.tarjetas[i].checked = true; 
    //     } else {
    //       this.tarjetas[i].checked = false;
    //     }
    //   }
    // });
    this.server.GetTarjetas(uid).then( data => {
      this.tarjetas = data;    
      let actualtarjeta = localStorage.getItem("tarjeta_actual") || 0;
      console.log("Tarjeta Actual",actualtarjeta);
      for (var i = 0; i < this.tarjetas.length; i++) {
        if(this.tarjetas[i].id==actualtarjeta) {
          this.tarjetas[i].checked = true; 
        } else {
          this.tarjetas[i].checked = false;
        }
      }
    })
  }

  deleteTarjeta(id: any){
      this.server.presentLoadingDefault("Eliminando...");
      //let user = this.auth.getUser();
      this.datos.uid = localStorage.uid;
      this.datos.id = id;
      this.server.removeTarjeta(this.datos).then((data)=>{
        this.server.dismissLoading();
        this.server.showAlert("Tarjeta Eliminada","Tarjeta eliminada con éxito");
        this.navCtrl.navigateBack("micuenta");
      }).catch((err)=>{
        this.server.dismissLoading();
        this.server.showAlert("Error al Eliminar","Ha ocurrido un error al eliminar tarjeta");
      });
  }

}
