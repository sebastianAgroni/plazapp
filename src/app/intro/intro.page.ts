import { Component, OnInit } from '@angular/core';
import { IonicModule, NavController, NavParams, MenuController } from '@ionic/angular'
import { EventEmitterService } from '../services/events/event-emitter.service';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {

  constructor(public navCtrl: NavController, 
              public menuCtrl: MenuController, 
              public event: EventEmitterService) {
    this.menuCtrl.enable(false, 'mimenu');
  }

  ngOnInit() {
    console.log('IntroPage');
    this.event.sendEmit();
    console.log('IntroPage after emit');
  }
  goReg(){
    // this.navCtrl.navigateForward("/registro");
    this.navCtrl.navigateForward("/verify-tel");
  }
  goLog(){
    this.navCtrl.navigateForward("/log"); 
  }
  goHome(){
    this.navCtrl.navigateForward("home");
  }

}
