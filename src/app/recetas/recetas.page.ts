import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { ParametrosService } from '../services/parametros.service';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../services/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-recetas',
  templateUrl: './recetas.page.html',
  styleUrls: ['./recetas.page.scss'],
})
export class RecetasPage implements OnInit {
  post: any;
  productoid: any;
	product: Array<{title: string, img: string}>;
	sliderConfig = {
	    slidesPerView: 3,
	    spaceBetween: 10,
	    centeredSlides: false
  	};

  constructor(public navCtrl: NavController,public param: ParametrosService,private auth: AuthService,public db: AngularFireDatabase) {
  	this.product = [
      {title: 'Frutal', img: 'receta1.jpg'},
      {title: 'Tropical', img: 'receta1.jpg'},
      {title: 'Detox', img: 'receta1.jpg'}
    ]
  }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.productoid = parseInt(localStorage.getItem("inner-product-select"));
    console.log("Recibiendo productoid: ", this.productoid);
    this.getPost();
  }

  getPost() {
    let me = this;
    this.auth.getGenOV('Post').then(data => {
      //me.post = data;
      let posts: any;
      posts = data;
      this.post = posts.filter(item => item.producto === this.productoid);
      localStorage.setItem('recetas',JSON.stringify(data));
      console.log("Post: ",this.post);
    })
  }

  goReceta(id: any) {
    localStorage.setItem("receta-select",id);
    this.navCtrl.navigateForward("receta");
  }

  goBack() {
    this.navCtrl.pop();
  }

}
