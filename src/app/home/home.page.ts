import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';

import { AuthService } from '../services/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs/Observable';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { ParametrosService } from '../services/parametros.service';
import { ServerService } from '../server/server.service';
import { EventEmitterService } from "./../services/events/event-emitter.service";
import { IonSlides } from '@ionic/angular';
import { ComponentsModule } from '../componentes/componentes.module';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
  categorias: any;
  productos: any;
  isloading = true;
  ctrlCat = false;
  urlimagen = 'http://cms.plaz.com.co/uploads/images/galeria/';
  sliders: any;
  user: any;
  nologin = false;
  valorCredito;
  FruitCoins = 0;
  Link = "";
  ctrlLink = this.Link.length;
  code;
  Ciudad;


  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    speed: 500,
    autoplay:true
  };

  constructor(
    private screenOrientation: ScreenOrientation,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    private auth: AuthService,
    public db: AngularFireDatabase,
    public loadingCtrl: LoadingController,
    public menuCtrl: MenuController,
    private param: ParametrosService,
    public platform: Platform,
    private server: ServerService,
    public event: EventEmitterService,
    private firebaseAnalytics: FirebaseAnalytics
  ) {
    this.menuCtrl.enable(true, 'mimenu');
    if (this.platform.is('cordova') ) {
      //this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }

    if (this.user === null) {
      this.nologin = false;
    }

    this.getSliders();
    this.getProductos();
    this.isloading = true;
    this.getCategorias();
    this.isloading = true;
    let cart = JSON.parse(localStorage.getItem('cart'));
    if (cart === undefined || cart === null || cart.length === 0) {
      cart = [];
      const numcart = [];
      const unitcart = [];
      const unittallacart = [];
      const badgecart = '0';
      const preciocart = [];
      const maduracioncart = [];
      localStorage.setItem('cart', JSON.stringify(cart));
      localStorage.setItem('numcart', JSON.stringify(numcart));
      localStorage.setItem('unitcart', JSON.stringify(unitcart));
      localStorage.setItem('unittallacart', JSON.stringify(unittallacart));
      localStorage.setItem('badgecart', badgecart);
      localStorage.setItem('preciocart', JSON.stringify(preciocart));
      localStorage.setItem('maduracioncart', JSON.stringify(maduracioncart));
      console.log('Cart is created', cart);
    } else {
      console.log('Cart already created', cart);
    }

    this.event.sendEmit();
    setTimeout(() => {
      var uid = localStorage.uid;
      this.Creditos(uid);
    }, 5000)

    //Borrar Codigo referido
    localStorage.removeItem("referido");
  }


  slidesDidLoad(slides: IonSlides) {
    slides.startAutoplay();
  }

  onInit(){}
  
  ionViewWillEnter() {
    localStorage.setItem('fromCart', 'false');
    localStorage.setItem('fromSearch', 'false');
    // this.FruitCoins = parseInt(localStorage.getItem('creditos'));
    // this.navCtrl.navigateForward("payload-done")
    const eventParams = {};
    this.firebaseAnalytics.logEvent('pagina_login', {page: "home"})
      .then(() => console.log('Event successfully tracked'))
      .catch(err => console.log('Error tracking event:', err));
  }

  async ionViewDidEnter(){
    //this.Creditos(null);
    this.TraeDatosUsuario();
    this.GetDirs();
    this.GetMoneda();
  }

  
  GetMoneda(){
    let tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Settings";
    this.auth.getGenKeyVal(tabla,"llave","moneda").then(data => {
      localStorage.moneda = data[0].valor;
    });
  }

  GetDirs(){
    let tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Envio";
    this.auth.getGenKeyVal(tabla,"uid",localStorage.uid).then(data => {
      localStorage.setItem("list-address",JSON.stringify(data));
      console.log("Direcciones: ",data);
      let address = localStorage.getItem("address");
    })
  }

  async TraeDatosUsuario(){
    let loading = await this.loadingCtrl.create();
    loading.present().then(() => {
      this.server.GetUserInfo(localStorage.uid).then(data => {
        console.log(data);
        if(typeof data[0] !== 'undefined'){
          localStorage.ciudad = data[0]['id_ciudad']
          localStorage.NombreCiudad = data[0]['ciudad'];
          localStorage.UnidadNegocio = data[0]['id_unidad'];
          localStorage.Cliente = data[0]['id_cliente'];
          this.Ciudad = data[0]['ciudad'];
          this.getProductos(data[0]['id_cliente']);
        }
        else{
          localStorage.ciudad = 1;
        }
        if(this.ctrlCat === false)
          //this.getCategorias();
        loading.dismiss();
      }).catch(error => {
        console.log("Error", error);
        loading.dismiss();
      })
    });
    
  }

  bienvenido() {
    this.navCtrl.navigateForward('/bienvenido');
  }

  intro() {
    this.navCtrl.navigateForward('/intro');
  }

  goCarrito() {
    if (!this.nologin) {
    this.navCtrl.navigateForward('/cart');
    } else{
      this.nologinAlert();
    }
  }

  goReg() {
    this.navCtrl.navigateForward('/registro');
  }

  goLog() {
    this.navCtrl.navigateForward('/log');
  }

  async goShop(id: any) {
    let loading = await this.loadingCtrl.create({});

    loading.present().then(() => {
      console.log('nologin', this.nologin);
      if (!this.nologin) {
        localStorage.setItem('cat-select', id);
        this.navCtrl.navigateRoot('tabs/shop');
      } else {
        this.nologinAlert();
      }
      loading.dismiss();
    });
  }

  getCategorias() {
    const me = this;
    var tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Categoria";
    this.auth.getGenOV(tabla).then(data => {
      me.categorias = this.sortJSON(data, 'orden', 'asc');
      localStorage.setItem('categorias', JSON.stringify(data));
      this.isloading = false;
      this.ctrlCat === true;
    });
  }

  getSliders() {
    var tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Imagengaleria";
    this.auth.getGenKeyVal(tabla, 'galeria', 1).then(data => {
      this.sliders = this.sortJSON(data, 'orden', 'asc');
      //console.log('Sliders: ', this.sliders);
    });
  }

  getProductos(cliente = null) {
    const me = this;
    var tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Producto";
    this.auth.getGenOV(tabla).then(data => {
      me.productos = data;
      localStorage.setItem('productos', JSON.stringify(data));
      console.log('Productos: ', me.productos);
      this.isloading = false;
    });
    // this.server.GetProductosPorUN(cliente).then(data => {
    //   this.productos = data;
    //   localStorage.setItem('productos', JSON.stringify(data));
    //   console.log('Productos: ', this.productos);
    //   this.isloading = false;
    // })
  }

  openMenu() {
    this.menuCtrl.toggle();
  }

  nologinAlert() {
    this.server.showAlert('', 'Para acceder debes estar registrado o logueado.');
  }

  async Creditos(uid){
    //let loading = await this.loadingCtrl.create();

    //loading.present().then(() => {
      
      this.server.getCreditos(uid).then((data) => {
        if(data['ok']){
          this.valorCredito = data['res'][0].valor;
          this.FruitCoins = data['res'][0].valor;
          localStorage.setItem("creditos", this.valorCredito);
          console.log("Créditos: ", this.FruitCoins)
          // loading.dismiss();
        }
        else{
          this.server.showAlert('Mensaje', 'No se pudieron obtener tus créditos');
          this.server.dismissLoading();
          // loading.dismiss();
        }
      }).catch(error => {
        console.log("error");
        // loading.dismiss();
      });
    // }).catch(error => {
    //   // console.log("error");
    //   loading.dismiss();
    // });
  }

  fruitcoins(){
    this.navCtrl.navigateForward('/mis-creditos');
  }

  sortJSON(data, key, orden) {
    return data.sort(function (a, b) {
        var x = a[key],
        y = b[key];

        if (orden === 'asc') {
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        }

        if (orden === 'desc') {
            return ((x > y) ? -1 : ((x < y) ? 1 : 0));
        }
    });
  }

  AbrirVentana(){
    window.open(this.Link, '_system');
    this.navCtrl.navigateRoot('historial');
  }


}
