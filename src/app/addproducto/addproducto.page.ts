import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { ParametrosService } from '../services/parametros.service';
import { Observable } from 'rxjs/Observable';
import { ServerService } from '../server/server.service';

import { AuthService } from '../services/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-addproducto',
  templateUrl: './addproducto.page.html',
  styleUrls: ['./addproducto.page.scss'],
})
export class AddproductoPage implements OnInit {
  pc = [];
  producto: any;
  productoid: any;
  tallas: any;
  inventario: any;
  num: any = 1;
  unit: any = '0';
  preciop: number;
  precio_producto ='0';
  cartcolor = 'primary';
  carttext = 'Agregar al carrito';
	checkboxs: Array<{val: string, isChecked: boolean}>;
  estadoproducto = 'Normal';
  maduracion = true;
  cantidad = 0;
  unittalla = '0';
  moneda = localStorage.moneda;
  ocultarPropiedades = 'false';

  constructor(public navCtrl: NavController,
    private auth: AuthService,
    public param: ParametrosService,
    public server: ServerService) {
  	this.checkboxs = [
      { val: 'Verde', isChecked: false },
      { val: 'Normal', isChecked: true },
      { val: 'Maduro', isChecked: false }
    ];
  }

  ngOnInit() {
    this.ocultarPropiedades = localStorage.getItem('ocultarPropiedades');
  }

  ionViewWillEnter() {
    console.log("this.estadoproducto", this.estadoproducto);
    this.server.presentLoadingDefault('Cargando Datos...');
    // Get ID product
    this.productoid = parseInt(localStorage.getItem('product-select'));
    console.log("productoid",this.productoid);
    // Get list of units
    let tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Talla";
    this.auth.getGenOV(tabla).then(data => {
      this.tallas = data;
    });
    // Get list from Inventario
    tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Inventario";
    this.auth.getGenKeyVal(tabla,'producto', this.productoid).then(data => {
      this.inventario = data;
      this.inventario = this.inventario.filter(item => item.precio !== 0);
      if(!this.inventario[0]){
        this.server.dismissLoading();
        this.server.showAlert("Producto no disponible","Lo sentimos este producto no esta disponible actualmente");
        this.navCtrl.pop();
        return 0;
      }
      this.precio_producto = this.inventario[0].precio;
      this.preciop = parseInt(this.inventario[0].precio);
      this.cantidad = parseInt(this.inventario[0].cantidad);
      localStorage.setItem('first_unit', this.inventario[0].id);
      // Detect if product was in cart
      const cart = JSON.parse(localStorage.getItem('cart'));
      const unitcart = JSON.parse(localStorage.getItem('unitcart'));
      const numcart = JSON.parse(localStorage.getItem('numcart'));
      const tallas = JSON.parse(localStorage.getItem('tallas'));
      const unittallacart = JSON.parse(localStorage.getItem('unittallacart'));
      const preciocart = JSON.parse(localStorage.getItem('preciocart'));
      const maduracioncart = JSON.parse(localStorage.getItem('maduracioncart')); 
      console.log(tallas);
      this.server.dismissLoading();
      if (cart.indexOf(this.productoid) == -1) {
        this.unit = localStorage.getItem('first_unit');
        this.num = 1;
        this.cartcolor = 'primary';
        this.carttext = 'Agregar al carrito';
      } else {
        this.unit = unitcart[cart.indexOf(this.productoid)];
        this.num = numcart[cart.indexOf(this.productoid)];
        this.cartcolor = 'danger';
        this.carttext = 'Agregado al carrito! ';
      }

      console.log(this.unit, "unnnnnnnnnnnnnnnnnnnnnnnnnnnnit");

      for(let c in cart){
        this.pc.push({
          "idProducto" : cart[c], 
          "idInventario" : unitcart[c], 
          "Maduracion" : maduracioncart[c], 
          "catidadProducto" : numcart[c],
          "numcart" : unittallacart[c],
          "PrecioProducto" : preciocart[c]
        });
      }
      this.Selection({val : "Normal", isChecked: false})
      this.checkboxs = [
        { val: 'Verde', isChecked: false },
        { val: 'Normal', isChecked: true },
        { val: 'Maduro', isChecked: false }
      ];
    });
    // Filter
    this.producto = JSON.parse(localStorage.getItem('productos'));
    this.producto = this.producto.filter(item => item.id === this.productoid)[0];
    // Have maduracion?
    console.log(this.producto);
    if (this.producto['maduracion']) {
      this.maduracion = false;
    } else {
      this.maduracion = true;
    }

    localStorage.setItem('producto-actual', JSON.stringify(this.producto));

  }

  Selection(item: any) {
    console.log("Entro a selection-->",item);
    console.log("productos en carro", this.pc);
    this.checkboxs.forEach(x => { x.isChecked = false; });
    console.log(this.checkboxs);
    this.estadoproducto = item.val;
    console.log("this.estadoproducto", this.estadoproducto);
    console.log("this.productoid", this.productoid);
    var ProductoActual = this.productoid+''+this.estadoproducto;
    for(let c in this.pc)
    {
      var x = this.pc[c]['idProducto']+''+this.pc[c]['Maduracion'];
      console.log("Comparacion ->", ProductoActual+'='+x);
      if(ProductoActual === x){
        this.unit = this.pc[c]['idInventario'];
        this.num = this.pc[c]['catidadProducto'];
        this.cartcolor = 'danger';
        this.carttext = 'Agregado al carrito! ';
        this.precio_producto = this.pc[c]['PrecioProducto'];
        return;
      }
      else{
        this.unit = localStorage.getItem('first_unit');
        this.num = 1;
        this.cartcolor = 'primary';
        this.carttext = 'Agregar al carrito';
        this.precio_producto = this.preciop+"";
      }
    }
    //console.log("preciop", this.preciop);
	}

  nameTalla(id: number) {
    const talla = this.tallas.filter(item => item.id === id);
    return talla[0].nombreEs;
  }

  onUnitChange(id: string) {
    const idp = parseInt(id);
    const precio = this.inventario.filter(item => item.id === idp);

    this.preciop = parseInt(precio[0].precio);
    this.cantidad = parseInt(precio[0].cantidad);
    this.unittalla = precio[0].talla;
    console.log('cantidad', this.cantidad);
    const numberp = parseInt(this.num);
    const totalp = this.preciop * numberp;
    this.precio_producto = String(totalp);
  }

  onNumChange(num: string) {
    const nump = parseInt(num);
    const totalp = this.preciop * nump;
    this.precio_producto = String(totalp);
  }

  addNum() {
    // if (this.num < this.cantidad) {
    //   this.num = parseInt(this.num) + 1;
    //   this.onNumChange(this.num);
    // } else {
    //   this.server.showAlert('', 'Superaste la cantidad máxima');
    // }
    this.num = parseInt(this.num) + 1;
    this.onNumChange(this.num);
  }

  rmNum() {
    if (this.num > 1) {
      this.num = parseInt(this.num) - 1;
      this.onNumChange(this.num);
    } else {
      this.server.showAlert('', 'No puedes escoger cantidad igual a 0');
    }
  }

  goRecetas(id: any) {
    localStorage.setItem('inner-product-select', id);
    this.navCtrl.navigateForward('recetas');
  }

  goProp(id: any) {
    localStorage.setItem('inner-product-select', id);
    this.navCtrl.navigateForward('propiedades');
  }

  addCart(id: any) {
    console.log("productos en carro", this.pc);
    //if (this.num <= this.cantidad) {
    console.log('num:', this.num);
    console.log('unit:', this.unit);
    console.log('precio:', this.preciop);
    const cart = JSON.parse(localStorage.getItem('cart'));
    const numcart = JSON.parse(localStorage.getItem('numcart'));
    const unitcart = JSON.parse(localStorage.getItem('unitcart'));
    const unittallacart = JSON.parse(localStorage.getItem('unittallacart'));
    const preciocart = JSON.parse(localStorage.getItem('preciocart'));
    const maduracioncart = JSON.parse(localStorage.getItem('maduracioncart'));
    console.log('carrito producto', cart.indexOf(id));
    var ProductoActual = this.productoid+''+this.estadoproducto;
    let suma = 0;
    for(let c in this.pc)
    {
      var x = this.pc[c]['idProducto']+''+this.pc[c]['Maduracion'];
      if(ProductoActual === x){
        suma ++;
      }
    }

    if(suma == 0){
      cart.push(id);
      numcart.push(this.num);
      unitcart.push(this.unit);
      unittallacart.push(this.unittalla);
      preciocart.push(this.precio_producto);
      maduracioncart.push(this.estadoproducto);
      // unitcarro.push(cart, unitcart);
      localStorage.setItem('cart', JSON.stringify(cart));
      localStorage.setItem('numcart', JSON.stringify(numcart));
      localStorage.setItem('unitcart', JSON.stringify(unitcart));
      localStorage.setItem('unittallacart', JSON.stringify(unittallacart));
      localStorage.setItem('preciocart', JSON.stringify(preciocart));
      localStorage.setItem('maduracioncart', JSON.stringify(maduracioncart));
      // localStorage.setItem('unitcarro', JSON.stringify(unitcarro));
      this.cartcolor = 'danger';
      this.carttext = 'Agregado al carrito';
    } else {
      var index = 0;
      for(let c in this.pc)
      {
        var x = this.pc[c]['idProducto']+''+this.pc[c]['Maduracion'];
        console.log("Comparacion ->", ProductoActual+'='+x);
        if(ProductoActual === x){
          index = parseInt(c);
        }
      }
      numcart[index] = this.num;
      unitcart[index] = this.unit;
      unittallacart[index] = this.unittalla;
      preciocart[index] = this.precio_producto;
      maduracioncart[index] = this.estadoproducto;
      localStorage.setItem('cart', JSON.stringify(cart));
      localStorage.setItem('numcart', JSON.stringify(numcart));
      localStorage.setItem('unitcart', JSON.stringify(unitcart));
      localStorage.setItem('unittallacart', JSON.stringify(unittallacart));
      localStorage.setItem('preciocart', JSON.stringify(preciocart));
      localStorage.setItem('maduracioncart', JSON.stringify(maduracioncart));
      this.cartcolor = 'danger';
      this.carttext = 'Agregado al carrito';
    }

    console.log('cart', cart);
    console.log('numcart', numcart);
    console.log('unitcart', unitcart);
    console.log('unittallacart', unittallacart);
    console.log('preciocart', preciocart);
    console.log('maduracioncart', maduracioncart);
    localStorage.setItem('badgecart', cart.length);
    this.goBack();
    // } else {
    //   this.server.showAlert('Cantidad Invalida','Lo sentimos, en este momento puedes agregar un máximo de '+ this.cantidad + " productos.");
    // }
  }

  goBack() {
    const from = localStorage.getItem('fromSearch');
    if (from == "true") {
      this.navCtrl.navigateBack('tabs/buscar');
    } else {
      this.navCtrl.navigateBack('tabs/shop');
    }
  }

}
