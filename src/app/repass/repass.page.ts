import { Component, OnInit } from '@angular/core';
import { NavController, Platform, AlertController  } from '@ionic/angular';
import { ServerService } from '../server/server.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-repass',
  templateUrl: './repass.page.html',
  styleUrls: ['./repass.page.scss'],
})
export class RepassPage implements OnInit {

  email = "";

  constructor(public navCtrl: NavController,public server: ServerService,private auth: AuthService) { }

  ngOnInit() {}

  repass(){
    let error: string = '';
    let hay_error : boolean = false;
    if(!this.server.validateEmail(this.email)){ error = 'Email inválido'; hay_error = true; }
    if (hay_error) {
      this.server.showAlert('Campos incorrectos',error);
    } else {
      this.server.presentLoadingDefault("Enviando email de recuperación...");
      this.auth.resetPassword(this.email).then((resp)=>{
        console.log("Enviando:",resp);
        this.server.dismissLoading();
        this.server.showAlert('Email de recuperación enviado','El correo de recuperación fue enviado al correo solicitado, revisa tu bandeja de entrada para restablecer la contraseña.'); 
        this.navCtrl.navigateBack("log");
      }).catch((err)=>{
        this.server.dismissLoading();
        this.server.showAlert('Error al enviar','Error al solicitar recuperación, intenta nuevamente!'); 
        console.log("error:",err);
      });
    } 
  }

  goReg(){
    this.navCtrl.navigateForward("registro");
  }

  goLog(){
    this.navCtrl.navigateBack("log");
  }

}
