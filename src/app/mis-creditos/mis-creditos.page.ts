import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { ParametrosService } from '../services/parametros.service';
import { ServerService } from '../server/server.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-mis-creditos',
  templateUrl: './mis-creditos.page.html',
  styleUrls: ['./mis-creditos.page.scss'],
})
export class MisCreditosPage implements OnInit {
  creditos;
  codigo;
  valorReferido = 0;
  referido = true;
  constructor(public navCtrl: NavController, 
    public menuCtrl: MenuController, 
    private auth: AuthService,
    public param: ParametrosService,
    public server : ServerService,
    private socialSharing: SocialSharing) { }

  ngOnInit() {
  }
  
  ionViewWillEnter(){
    console.log(this.creditos);
    this.creditos = localStorage.getItem("creditos");
    const uid = localStorage.uid;
    this.Creditos(uid);
    console.log(uid);

    this.codigo = uid.substr(5,8);
    let tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Settings";
    this.auth.getGenKeyVal(tabla, "llave", "credito_referidos").then(data => {
      console.log(data);
      this.valorReferido = data[0]['valor'];
    }).catch(error => {
      console.log(error);
    })

  }

  openMenu() {
    this.menuCtrl.toggle();
  }

  Creditos(uid){
    this.server.getCreditos(uid).then((data) => {
      if(data['ok']){
        this.creditos = data['res'][0].valor;
        localStorage.setItem("creditos", this.creditos);
      }
      else{
        this.server.showAlert('Mensaje', 'No se pudieron obtener tus créditos');
        this.server.dismissLoading();
      }
    });
  }

  sendShare() {
    let message = "Hola, regístrate en Plaz con este codigo "+this.codigo+" y obten $"+this.valorReferido+" para tu primer mercado. 🍐🍎🍇🍅🥬"
    //let message = "Hola, te regalo $"+this.valorReferido+" para que utilices en tu primera compra en PLAZ usa este código "+this.codigo+" al registrarte";
    let subject = "Un regalo de PLAZ";
    let url = "https://somosplaz.com/";
    this.socialSharing.share(message, subject, null, url);
  }

}
