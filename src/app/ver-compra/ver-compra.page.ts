import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user-service';
import { ServerService } from '../server/server.service';
@Component({
  selector: 'app-ver-compra',
  templateUrl: './ver-compra.page.html',
  styleUrls: ['./ver-compra.page.scss'],
})
export class VerCompraPage implements OnInit {
  username;
  compra;
  total;
  status;
  idGen;
  num;
  evento;
  productos: any;
  metodo;
  fecha;
  comentarios: string = "";
  propina = 0;
  bolsa = "No";
  moneda = localStorage.moneda;


  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController,
    private user: UserService,
    private auth: AuthService,
    public server: ServerService,
    private alertCtrl: AlertController) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    //Get User
    let idcompra = parseInt(localStorage.getItem("idcompra"));
    console.log(idcompra);
    //this.getProducto(idcompra);
    this.getinfoProducto(idcompra);
    //Get Products
  }

  goBack() {
    this.navCtrl.navigateBack("historial");
  }

  getProducto(id: number) {
    this.user.getCurrentUser()
    .then(user => {
      this.auth.getGenKeyVal("Compra","id",id).then(data => {
        this.compra = data;
        this.total = data[0].total;
        this.metodo = data[0].metodo;
        this.fecha = data[0].createdAt;
        this.propina = data[0].propina;
        this.comentarios = data[0].message;
        if(data[0].bolsa){
          this.bolsa = "Si";
        } else {
          this.bolsa = "No";
        }
        // if (data[0].estado==4) {
        // 	this.status = "Compra Aprobada";
        // }
        this.status = this.metodo;
        this.idGen = data[0].guid;
        let idcompra = data[0].id;
        this.auth.getGenKeyVal("Compraitem","compra",idcompra).then(data2 => {
          let datacompraitem: any = data2;
	        //this.productos = data2;
          for (let i = 0; i < datacompraitem.length; i++) {
            let producto = parseInt(datacompraitem[i].producto);
            this.auth.getGenKeyVal("Producto","id",producto).then(data3 => {
                datacompraitem[i].nombre = data3[0].nombreEs;
            });
            let talla = parseInt(datacompraitem[i].talla);
            this.auth.getGenKeyVal("Talla","id",talla).then(data3 => {
                datacompraitem[i].unidad = data3[0].nombreEs;
            });
          }
          this.productos = datacompraitem;
	      });
      })
    });
  }

  getinfoProducto(idCompra: number){
    // let user = this.user.getCurrentUser();
    // this.user.getCurrentUser().then(user => {
    let CompraSelect = JSON.parse(localStorage.CompraSelect);
    console.log(CompraSelect);
    this.server.getHistorialDetalle(idCompra, localStorage.UnidadNegocio).then((data)=>{
      let resp: any = data;
      if(resp.ok){
        this.fecha = CompraSelect.fecha;
        this.metodo = CompraSelect.metodo;
        this.bolsa = CompraSelect.bolsa;
        this.propina = CompraSelect.propina;
        this.total = CompraSelect.total;
        this.comentarios = CompraSelect.message;
        //this.status = this.compra.status;
        this.status = this.metodo;

        this.productos = resp.compraitem;

        localStorage.setItem('compraC',JSON.stringify(CompraSelect));
        localStorage.removeItem("CompraSelect");
        localStorage.setItem('productosC',JSON.stringify(resp.compraitem));
      } else{
        this.server.showAlert('Mensaje',"No encontramos informacion de este pedido.");
        this.navCtrl.navigateForward("historial");
      }
      }).catch((err)=>{
        console.log(err);
        this.server.showAlert('Mensaje',"No encontramos informacion de este pedido.");
        this.navCtrl.navigateForward("historial");
      });
    // });
  }

  addCart(){
    let productos = localStorage.getItem("productosC");
    let prod = JSON.parse(productos);
    console.log("Compra desde el localstorage", JSON.parse(productos));
    const cart = []
    const numcart = []
    const unitcart = []
    const unittallacart = []
    const preciocart = []
    const maduracioncart = []
    localStorage.setItem('cart', JSON.stringify(cart));
    localStorage.setItem('numcart', JSON.stringify(numcart));
    localStorage.setItem('unitcart', JSON.stringify(unitcart));
    localStorage.setItem('unittallacart', JSON.stringify(unittallacart));
    localStorage.setItem('preciocart', JSON.stringify(preciocart));
    localStorage.setItem('maduracioncart', JSON.stringify(maduracioncart));

    let pi = 0;
    let total = 0;
    for(let i = 0; i < prod.length ; i++)
    {
      console.log("Producto", prod[i]);
      let p = prod[i];

      if(p.Activo == 1)
      {
        cart.push(parseInt(p.ProductoId));
        numcart.push(p.cantidad);
        unitcart.push(p.InventarioId);
        unittallacart.push(p.TallaId);
        preciocart.push(p.PrecioProducto);
        maduracioncart.push(p.Maduracion);
        total ++;
      }
      else{
        pi++;
      }
    }

    localStorage.setItem('cart', JSON.stringify(cart));
    localStorage.setItem('numcart', JSON.stringify(numcart));
    localStorage.setItem('unitcart', JSON.stringify(unitcart));
    localStorage.setItem('unittallacart', JSON.stringify(unittallacart));
    localStorage.setItem('preciocart', JSON.stringify(preciocart));
    localStorage.setItem('maduracioncart', JSON.stringify(maduracioncart));

    localStorage.setItem('badgecart', total+"");
    if(pi > 0)
      this.server.showAlert('Mensaje','Omitimos '+pi+' producto(s) debido a que no están disponibles en el momento!');
    this.navCtrl.navigateBack('tabs/home');
  }

  // addCart(id: any) {
  //   console.log(this.num +'='+ this.cantidad);
  //   if (this.num <= this.cantidad) {
  //   console.log('num:', this.num);
  //   console.log('unit:', this.unit);
  //   console.log('precio:', this.preciop);
  //   const cart = JSON.parse(localStorage.getItem('cart'));
  //   const numcart = JSON.parse(localStorage.getItem('numcart'));
  //   const unitcart = JSON.parse(localStorage.getItem('unitcart'));
  //   const unittallacart = JSON.parse(localStorage.getItem('unittallacart'));
  //   const preciocart = JSON.parse(localStorage.getItem('preciocart'));
  //   const maduracioncart = JSON.parse(localStorage.getItem('maduracioncart'));
  //   console.log('carrito producto', cart.indexOf(id));
  //   if (cart.indexOf(id) == -1) {
  //     cart.push(id);
  //     numcart.push(this.num);
  //     unitcart.push(this.unit);
  //     unittallacart.push(this.unittalla);
  //     preciocart.push(this.precio_producto);
  //     maduracioncart.push(this.estadoproducto);
  //     localStorage.setItem('cart', JSON.stringify(cart));
  //     localStorage.setItem('numcart', JSON.stringify(numcart));
  //     localStorage.setItem('unitcart', JSON.stringify(unitcart));
  //     localStorage.setItem('unittallacart', JSON.stringify(unittallacart));
  //     localStorage.setItem('preciocart', JSON.stringify(preciocart));
  //     localStorage.setItem('maduracioncart', JSON.stringify(maduracioncart));
  //     this.cartcolor = 'danger';
  //     this.carttext = 'Agregado al carrito';
  //   } else {
  //     const index = cart.indexOf(id);
  //     numcart[index] = this.num;
  //     unitcart[index] = this.unit;
  //     unittallacart[index] = this.unittalla;
  //     preciocart[index] = this.precio_producto;
  //     maduracioncart[index] = this.estadoproducto;
  //     localStorage.setItem('cart', JSON.stringify(cart));
  //     localStorage.setItem('numcart', JSON.stringify(numcart));
  //     localStorage.setItem('unitcart', JSON.stringify(unitcart));
  //     localStorage.setItem('unittallacart', JSON.stringify(unittallacart));
  //     localStorage.setItem('preciocart', JSON.stringify(preciocart));
  //     localStorage.setItem('maduracioncart', JSON.stringify(maduracioncart));
  //     this.cartcolor = 'danger';
  //     this.carttext = 'Agregado al carrito';
  //   }
  //   console.log('cart', cart);
  //   console.log('numcart', numcart);
  //   console.log('unitcart', unitcart);
  //   console.log('unittallacart', unittallacart);
  //   console.log('preciocart', preciocart);
  //   console.log('maduracioncart', maduracioncart);
  //   localStorage.setItem('badgecart', cart.length);
  //   this.goBack();
  //   } else {
  //     this.server.showAlert('Cantidad Invalida','Lo sentimos, en este momento puedes agregar un máximo de '+ this.cantidad + " productos.");
  //   }
  // }


}
