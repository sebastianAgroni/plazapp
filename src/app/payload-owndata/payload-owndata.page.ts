import { Component, OnInit } from '@angular/core';
import { ServerService } from '../server/server.service';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user-service';


@Component({
  selector: 'app-payload-owndata',
  templateUrl: './payload-owndata.page.html',
  styleUrls: ['./payload-owndata.page.scss'],
})
export class PayloadOwndataPage implements OnInit {
  Nombre;
  Celular;
  Correo;
  TipoDocumento = "1";
  Documento;
  address;
  Direccion;
  val;

  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController,
    public server: ServerService,
    public loadingCtrl : LoadingController,
    private auth: AuthService,
    public user : UserService) { }

  ngOnInit() {
    localStorage.descuento = "0";
  }
  
  async ionViewWillEnter(){
    //localStorage.address = "0";
    let loading = await this.loadingCtrl.create({
    });
    loading.present().then(() => {
      this.server.getUserByuid(localStorage.uid).then(d => {
        
        let user = d['data'][0];
        console.log(user);
        this.Nombre = user.nombre;
        this.Celular = user.telefono;
        this.Correo = user.email;
        if(user.tipo_documento){
          this.TipoDocumento = String(user.tipo_documento);
          this.Documento = user.documento;
        }
        this.address = JSON.parse(localStorage.getItem("list-address"));
        this.Direccion = !localStorage.address? "0" : String(localStorage.address);
        if(localStorage.address && localStorage.address.indexOf("~") < 0)
        {
          this.Direccion = "0";
        }
        loading.dismiss();
      }).catch(error => {
        console.log(error);
        loading.dismiss();
        this.server.showAlert("Mensaje", "No se pudieron obtener tus datos");
        this.navCtrl.navigateForward("tabs/home");
        return;
    });
  });
}

  async ValidaInfo(){ 
    // validacion nombre
    if(!this.Nombre){
      this.server.showAlert("Mensaje", "El nombre es obligatorio");
      return;
     }
     var nombreCompleto = this.Nombre.split(" ");
     if(nombreCompleto.length < 2){
      this.server.showAlert("Mensaje", "Debes ingresar nombre y apellido");
      return;
     }
    // validacion correo
    if(!this.Correo){
     this.server.showAlert("Mensaje", "El correo es obligatorio");
     return;
    }
    let corr = this.server.validateEmail(this.Correo);
    if(!corr){
      this.server.showAlert("Mensaje", "El correo es incorrecto");
      return;
    }

    // Validar Celular
    this.Celular = String(this.Celular);
    console.log(this.Celular)

    if(!this.Celular){
      this.server.showAlert("Mensaje", "Número celular es obligatorio");
      return;
    }

    // if(this.Celular.length != 10 || this.Celular[0] != 3) {
    //   this.server.showAlert("Mensaje", "Número de celular no válido");
    //   return;
    // }

    // Validar Documento de identidad
    if(!this.Documento){
      this.server.showAlert("Mensaje", "Número de documento es obligatorio");
      return;
    }

    if(this.Documento.length <= 3){
      this.server.showAlert("Mensaje", "Número de documento no válido");
      return;
    }

    //Validaciones direcciones 
    var dirUsuario = JSON.parse(localStorage.getItem("list-address"));
    console.log(this.Direccion, this.Direccion.split("~")[0]);
    if(parseInt(this.Direccion.split("~")[0]) > 0){
      let dir = dirUsuario.find(x => parseInt(x.id) === parseInt(this.Direccion.split("~")[0]));
      console.log("Direccion es:", dir);
      if(dir === undefined){
        this.Direccion = "0";
        this.server.showAlert("Mensaje", "Debes seleccionar una dirección");
        return;
      }
    }

    if(this.Direccion === "0" || !this.Direccion){
      this.server.showAlert("Mensaje", "Debes seleccionar una dirección");
      return;
    }


    let user = {
      "uid" : localStorage.uid,
      "email" : this.Correo,
      "nombre" : this.Nombre,
      "telefono" : this.Celular,
      "tipo_documento" : this.TipoDocumento,
      "documento" : this.Documento
    };


    let loading = await this.loadingCtrl.create({
      //message: 'Please wait...',
    });
    loading.present().then(() => {
      this.server.setUserbyId(user).then(data => {
        loading.dismiss();
        this.navCtrl.navigateForward("tipo-pago");
        localStorage.address = String(this.Direccion);
        localStorage.Dir = String(this.Direccion.split("~")[1]);
      }).catch(error =>{
        loading.dismiss();
        this.server.showAlert("Mensaje", "No se pudieron guardar tus datos");
        this.navCtrl.navigateForward("tabs/home");
        return;
      });
    });
  }

  AgregarDirecion(){
    this.navCtrl.navigateForward("add-adress-geo");
    return;
  }

  goBack(){
    this.navCtrl.navigateForward("payload-program");
  }

}
