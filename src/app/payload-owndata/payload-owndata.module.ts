import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PayloadOwndataPage } from './payload-owndata.page';

const routes: Routes = [
  {
    path: '',
    component: PayloadOwndataPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PayloadOwndataPage]
})
export class PayloadOwndataPageModule {}
