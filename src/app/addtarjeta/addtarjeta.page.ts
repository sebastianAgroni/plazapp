import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';

import { AuthService } from '../services/auth.service';
import { ServerService } from "../server/server.service";
import { UserService } from '../services/user-service';
import { ReturnStatement } from '@angular/compiler';

declare var Payment;

@Component({
  selector: 'app-addtarjeta',
  templateUrl: './addtarjeta.page.html',
  styleUrls: ['./addtarjeta.page.scss'],
})
export class AddtarjetaPage implements OnInit {

	datos = {
    "titular": "",
    "numDoc": "",
    "numeroTarjeta": "",
    "cvv": "",
    "cuotas_new": 1,
    "fecha": "",
    "direccion": "",
    "email": "",
    "uid": "",
    "tipoTarjeta": "",
    "tipoDoc": "",
    "total": 0,
    "impuesto": 0,
    "plan": 0,
    "telefono": "",
    "cupon":"",
    "cupon_id":""
  };
  today;
  manana;

  constructor(private auth: AuthService, 
    public menuCtrl: MenuController, 
    public server: ServerService, 
    public nav: NavController,
    public userService: UserService,
    public navCtrl: NavController) {
	  	this.today=new Date().toISOString();
	    var d = new Date();
	    var year = d.getFullYear();
	    var month = d.getMonth();
	    var day = d.getDate();
      this.manana = new Date((year + 10), month, day ).toISOString();
  }

  ngOnInit() {
  }

  addTarjeta(){
    console.log(this.datos);

    let data = this.datos;
    let error: string = '';
    let hay_error : boolean = false;
    if(data.direccion.length < 6){
      error = 'Debes ingresar una dirección de al menos 3 caracteres';
      hay_error = true;
    }
    if( ((this.getCardType(data.numeroTarjeta) != "AMEX") && data.cvv.length != 3) || ( (this.getCardType(data.numeroTarjeta) == "AMEX") && (data.cvv.length != 4) )){
      error = 'Debes ingresar un código de verificación de 3 caracteres';
      if(this.getCardType(data.numeroTarjeta)=="AMEX")
        error = 'Debes ingresar un código de verificación de 4 caracteres';
      hay_error = true;
      if(isNaN(data.cvv.length)){
        error = 'Debes ingresar un código de verificación de solo números';
        hay_error = true;
      }
    }
    if(data.fecha.length < 1){
      error = 'Debes seleccionar la fecha de vencimiento';
      hay_error = true;
    }
    let dataFecha = data.fecha.split("-");
    data.fecha = dataFecha[0]+"-"+dataFecha[1];
    const fechaselect = new Date(data.fecha);
    //data.fecha = (fechaselect.getFullYear() +'-'+ (fechaselect.getMonth()<10?'0':'') + fechaselect.getMonth());
    console.log(data.fecha);
    //return;
    if(data.numeroTarjeta.length < 1){
      error = 'Debes ingresar el número de tarjeta de credito';
      hay_error = true;
    }else{
      if(this.getCardType(data.numeroTarjeta) == ""){
        error = 'El número de tarjeta es inválido';
        hay_error = true;
      }else{
        this.datos.tipoTarjeta = this.getCardType(data.numeroTarjeta);
      }
    }
    if(data.telefono.length < 1){
      error = 'Debes ingresar el numero de celular del titular';
      hay_error = true;
    }
    if(data.numDoc.length < 6){
      error = 'El número de documento no puede tener menos de 6 caracteres';
      hay_error = true;
    }
    if(!this.isAlpha(data.titular)){
      // error = 'El nombre del titular de la tarjeta solo puede contener letras, no puede tener espacios';
      error = 'El nombre del titular de la tarjeta no puede tener espacios, números ni carácteres especiales';
      hay_error = true;
    }
    if(data.titular.length < 3){
      error = 'Debes ingresar el nombre del titular de la tarjeta';
      hay_error = true;
    }
    if(hay_error){
      data.fecha = "";
      this.server.showAlert('Error',error);
      return;
    }
    else{
      this.server.presentLoadingDefault("Procesando tarjeta...");
      //let user = this.auth.getUser();
      let user = JSON.parse(localStorage.user)
      this.datos.email = user.email;
      this.datos.uid = user.uid;
      this.datos.tipoDoc = 'CEDULA';
      /****************************paymentes*****************************/
        // Payment.init('stg', 'PLAZ-CO-CLIENT', 'SoT8VMxIX7L2FrAPOUCAOvh7weTU1k');

        // var cardToSave = {
        //   "card": {
        //       "number": this.datos.numeroTarjeta,
        //       "holder_name": this.datos.titular,
        //       "expiry_month": parseInt(this.datos.fecha.split("-")[1]),
        //       "expiry_year": parseInt(this.datos.fecha.split("-")[0]),
        //       "cvc": this.datos.cvv,
        //   }
        // };

        // var uid = this.datos.uid;
        // var email = this.datos.email;
        // Payment.addCard(uid, email, cardToSave, this.successHandler, this.errorHandler);
        // this.server.dismissLoading();
      /****************************paymentes*****************************/

      this.server.addTarjeta(this.datos).then(async (data)=>{
        this.server.dismissLoading();
        if (data["ok"]) {
          this.datos['UnidadNegocio'] = localStorage.UnidadNegocio;
          await this.server.AddTarjeta2(JSON.stringify(this.datos))
          .then(data => console.log(data))
          .catch(error => console.error(error));
          this.server.showAlert("Tarjeta Agregada","Tarjeta agregada con éxito");
          this.navCtrl.navigateBack("micuenta");
        } else {
          this.server.showAlert("Error al Agregar",data["error_msg"]);
          return;
        }
      }).catch((err)=>{
        this.server.dismissLoading();
        this.server.showAlert("Error al Agregar","Ha ocurrido un error al agregar tarjeta");
      });

    }

  }

  // successHandler = function (cardResponse) {
  //   console.log(cardResponse.card);
  // };

  // errorHandler = function (err) {
  //   let errno = err.error.type;
  //   if(errno.indexOf("already added") === 0){
  //     this.server.showAlert("Mensaje","Tarjeta ya agregada anteriormente!!");
  //   }
  // };

  isAlpha(name) {
    let regex = /[a-zA-Z]+$/ ;
    console.log("regex:",name,regex.test(name));
    return regex.test(name);
  }


  getCardType(cardNum) {
    if(!this.luhnCheck(cardNum)){
	    return "";
    }
    var payCardType = "";
    var regexMap = [
      {regEx: /^4[0-9]{5}/ig,cardType: "VISA"},
      {regEx: /^5[1-5][0-9]{4}/ig,cardType: "MASTERCARD"},
      {regEx: /^3[47][0-9]{3}/ig,cardType: "AMEX"},
      {regEx: /^(5[06-8]\d{4}|6\d{5})/ig,cardType: "MAESTRO"}
    ];
    for (var j = 0; j < regexMap.length; j++) {
      if (cardNum.match(regexMap[j].regEx)) {
        payCardType = regexMap[j].cardType;
        break;
      }
    }
    if (cardNum.indexOf("50") === 0 || cardNum.indexOf("60") === 0 || cardNum.indexOf("65") === 0) {
      var g = "508500-508999|606985-607984|608001-608500|652150-653149";
      var i = g.split("|");
      for (var d = 0; d < i.length; d++) {
        var c = parseInt(i[d].split("-")[0], 10);
        var f = parseInt(i[d].split("-")[1], 10);
        if ((cardNum.substr(0, 6) >= c && cardNum.substr(0, 6) <= f) && cardNum.length >= 6) {
         payCardType = "RUPAY";
          break;
        }
      }
    }
    return payCardType;
  }
	
	
	luhnCheck(cardNum){
	    var numericDashRegex = /^[\d\-\s]+$/
	    if (!numericDashRegex.test(cardNum)) return false;
	    var nCheck = 0, nDigit = 0, bEven = false;
	    var strippedField = cardNum.replace(/\D/g, "");
	    for (var n = strippedField.length - 1; n >= 0; n--) {
	        var cDigit = strippedField.charAt(n);
	        nDigit = parseInt(cDigit, 10);
	        if (bEven) {
	            if ((nDigit *= 2) > 9) nDigit -= 9;
	        }
	        nCheck += nDigit;
	        bEven = !bEven;
	    }
	  	return (nCheck % 10) === 0;
	}

  goBack() {
    this.navCtrl.pop();
  }
}
