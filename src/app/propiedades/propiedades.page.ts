import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';

import { ParametrosService } from '../services/parametros.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-propiedades',
  templateUrl: './propiedades.page.html',
  styleUrls: ['./propiedades.page.scss'],
})
export class PropiedadesPage implements OnInit {

	producto: any;

  constructor(public param: ParametrosService,public navCtrl: NavController) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
  	this.producto = JSON.parse(localStorage.getItem('producto-actual'));
  	console.log("producto",this.producto);
  }

  goBack() {
    this.navCtrl.pop();
  }

}
