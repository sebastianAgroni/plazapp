import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { SearchService } from '../services/search.service';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import { ParametrosService } from '../services/parametros.service';
var BuscarPage = /** @class */ (function () {
    function BuscarPage(navCtrl, searchservice, param, menuCtrl) {
        this.navCtrl = navCtrl;
        this.searchservice = searchservice;
        this.param = param;
        this.menuCtrl = menuCtrl;
        this.searchTerm = '';
        this.searching = false;
        this.searchControl = new FormControl();
    }
    BuscarPage.prototype.ngOnInit = function () {
    };
    BuscarPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.setFilteredItems();
        this.searchControl.valueChanges.debounceTime(700).subscribe(function (search) {
            _this.searching = false;
            _this.setFilteredItems();
        });
    };
    BuscarPage.prototype.setFilteredItems = function () {
        this.items = this.searchservice.filterItems(this.searchTerm);
        this.searching = true;
    };
    BuscarPage.prototype.selectBuscar = function (id) {
        this.param.dataProducto(id);
        localStorage.setItem("fromSearch", "true");
        this.navCtrl.navigateForward("addproducto");
    };
    BuscarPage.prototype.openMenu = function () {
        this.menuCtrl.toggle();
    };
    BuscarPage = tslib_1.__decorate([
        Component({
            selector: 'app-buscar',
            templateUrl: './buscar.page.html',
            styleUrls: ['./buscar.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController, SearchService, ParametrosService, MenuController])
    ], BuscarPage);
    return BuscarPage;
}());
export { BuscarPage };
//# sourceMappingURL=buscar.page.js.map