import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { firebaseConfig } from '../config';
import { AuthService } from './services/auth.service';
import { ParametrosService } from './services/parametros.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { FormsModule } from '@angular/forms';
import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';
import { TabsPageModule } from './tabs/tabs.module';
import { Facebook } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ServerService } from "./server/server.service";
import { UserService } from './services/user-service';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib_1.__decorate([
        NgModule({
            declarations: [AppComponent],
            entryComponents: [],
            imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,
                AngularFireModule.initializeApp(firebaseConfig.fire),
                AngularFireDatabaseModule, FormsModule, TabsPageModule, HttpClientModule],
            providers: [
                CallNumber,
                Keyboard,
                StatusBar,
                SplashScreen,
                { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
                AngularFireAuth,
                AngularFireDatabase,
                AuthService,
                ScreenOrientation,
                FirebaseAuthentication,
                ParametrosService,
                Facebook,
                GooglePlus,
                HttpClient,
                ServerService,
                UserService
            ],
            bootstrap: [AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map