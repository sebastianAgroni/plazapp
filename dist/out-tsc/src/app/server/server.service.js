import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserService } from '../services/user-service';
import { LoadingController, AlertController } from '@ionic/angular';
import * as firebase from 'firebase/app';
import 'rxjs/add/operator/map';
var ServerService = /** @class */ (function () {
    function ServerService(loadingCtrl, http, userService, alertCtrl) {
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.userService = userService;
        this.alertCtrl = alertCtrl;
        this.apipath = 'http://cms.plaz.com.co/api/';
        this.registerUser = this.apipath + 'registrar-usuario';
        this.updateUsuario = this.apipath + 'update-usuario';
        this.agregarDireccion = this.apipath + 'add-direccion';
        this.agregarTarjeta = this.apipath + 'agregar-tarjeta';
        this.newPedido = this.apipath + 'secure/pedido';
    }
    ServerService.prototype.createNewUser = function (register, name, tel) {
        var _this = this;
        this.presentLoadingDefault('Autenticando');
        console.log("Creando Nuevo Usuario");
        this.userService.getCurrentUser()
            .then(function (user) {
            var headers = new HttpHeaders();
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
            headers.append('Accept', 'application/json');
            headers.append('content-type', 'application/json');
            var requestOptions = { headers: headers };
            _this.http.post(_this.registerUser, user, requestOptions)
                .subscribe(function (data) {
                if (register) {
                    user.name = name;
                    user.telefono = tel;
                    console.log("update user", user);
                    _this.updateUser(user);
                }
                _this.dismissLoading();
            }, function (error) {
                _this.dismissLoading();
            });
        }, function (err) {
            _this.dismissLoading();
        });
    };
    ServerService.prototype.updateUser = function (user) {
        var _this = this;
        this.presentLoadingDefault('Actualizando Usuario...');
        return new Promise(function (resolve, reject) {
            var headers = new HttpHeaders();
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
            headers.append('Accept', 'application/json');
            headers.append('content-type', 'application/json');
            var requestOptions = { headers: headers };
            _this.http.post(_this.updateUsuario, user, requestOptions).subscribe(function (data) {
                console.log(data);
                var userfb = firebase.auth().currentUser;
                userfb.updateProfile({
                    displayName: user.name
                });
                localStorage.setItem("phoneUser", user.telefono);
                _this.dismissLoading();
                resolve(data);
            }, function (error) {
                console.log(error);
                _this.dismissLoading();
                reject(error.error);
            });
        });
    };
    ServerService.prototype.addDireccion = function (direccion) {
        var _this = this;
        this.presentLoadingDefault('Creando Dirección');
        return new Promise(function (resolve, reject) {
            var headers = new HttpHeaders();
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
            headers.append('Accept', 'application/json');
            headers.append('content-type', 'application/json');
            var requestOptions = { headers: headers };
            _this.http.post(_this.agregarDireccion, direccion, requestOptions)
                .subscribe(function (data) {
                console.log(data);
                resolve(data);
                _this.dismissLoading();
            }, function (error) {
                console.log(error);
                reject(error);
                _this.dismissLoading();
            });
        });
    };
    ServerService.prototype.addTarjeta = function (datos) {
        var _this = this;
        this.presentLoadingDefault('Creando Dirección');
        return new Promise(function (resolve, reject) {
            var headers = new HttpHeaders();
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
            headers.append('Accept', 'application/json');
            headers.append('content-type', 'application/json');
            var requestOptions = { headers: headers };
            _this.http.post(_this.agregarTarjeta, datos, requestOptions)
                .subscribe(function (data) {
                console.log(data);
                resolve(data);
                _this.dismissLoading();
            }, function (error) {
                console.log(error);
                reject(error);
                _this.dismissLoading();
            });
        });
    };
    ServerService.prototype.addPedido = function (datos) {
        var _this = this;
        this.presentLoadingDefault('Realizando Pedido');
        return new Promise(function (resolve, reject) {
            var headers = new HttpHeaders();
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
            headers.append('Accept', 'application/json');
            headers.append('content-type', 'application/json');
            var requestOptions = { headers: headers };
            _this.http.post(_this.newPedido, datos, requestOptions)
                .subscribe(function (data) {
                console.log(data);
                resolve(data);
                _this.dismissLoading();
            }, function (error) {
                console.log(error);
                reject(error);
                _this.dismissLoading();
            });
        });
    };
    ServerService.prototype.geoCode = function (dir) {
        var _this = this;
        this.presentLoadingDefault('Localizando Dirección...');
        return new Promise(function (resolve, reject) {
            dir = dir.replace(/[\W_]+/g, " ");
            dir = dir.replace(/ /g, "+");
            var url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + dir + "&key=AIzaSyCpXAChhUJHccLmv3sE-FM8enJ79lkS4F0";
            console.log("url", url);
            _this.http.get(url).subscribe(function (data) {
                console.log(data);
                _this.dismissLoading();
                resolve(data);
            }, function (error) {
                console.log(error);
                _this.dismissLoading();
                reject(error.error);
            });
        });
    };
    ServerService.prototype.presentLoadingDefault = function (title) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.loadingCtrl.create({
                                spinner: 'circles',
                                message: title,
                                translucent: true,
                                cssClass: 'custom-class custom-loading'
                            })];
                    case 1:
                        _a.loading = _b.sent();
                        return [4 /*yield*/, this.loading.present()];
                    case 2: return [2 /*return*/, _b.sent()];
                }
            });
        });
    };
    ServerService.prototype.showAlert = function (titulo, mensaje) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alertct;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: titulo,
                            message: mensaje,
                            buttons: ['OK']
                        })];
                    case 1:
                        alertct = _a.sent();
                        return [4 /*yield*/, alertct.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ServerService.prototype.dismissLoading = function () {
        this.loading.dismiss();
    };
    ServerService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [LoadingController,
            HttpClient,
            UserService,
            AlertController])
    ], ServerService);
    return ServerService;
}());
export { ServerService };
//# sourceMappingURL=server.service.js.map