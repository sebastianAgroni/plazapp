import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PayloadProgramPage } from './payload-program.page';
var routes = [
    {
        path: '',
        component: PayloadProgramPage
    }
];
var PayloadProgramPageModule = /** @class */ (function () {
    function PayloadProgramPageModule() {
    }
    PayloadProgramPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PayloadProgramPage]
        })
    ], PayloadProgramPageModule);
    return PayloadProgramPageModule;
}());
export { PayloadProgramPageModule };
//# sourceMappingURL=payload-program.module.js.map