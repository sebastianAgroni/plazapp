import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { ServerService } from "../server/server.service";
import { UserService } from '../services/user-service';
var AddtarjetaPage = /** @class */ (function () {
    function AddtarjetaPage(auth, menuCtrl, server, nav, userService, navCtrl) {
        this.auth = auth;
        this.menuCtrl = menuCtrl;
        this.server = server;
        this.nav = nav;
        this.userService = userService;
        this.navCtrl = navCtrl;
        this.datos = {
            "titular": "",
            "numDoc": "",
            "numeroTarjeta": "",
            "cvv": "",
            "cuotas_new": 1,
            "fecha": "",
            "direccion": "",
            "email": "",
            "uid": "",
            "tipoTarjeta": "",
            "tipoDoc": "",
            "total": 0,
            "impuesto": 0,
            "plan": 0,
            "telefono": "",
            "cupon": "",
            "cupon_id": ""
        };
        this.today = new Date().toISOString();
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth();
        var day = d.getDate();
        this.manana = new Date((year + 10), month, day).toISOString();
    }
    AddtarjetaPage.prototype.ngOnInit = function () {
    };
    AddtarjetaPage.prototype.addTarjeta = function () {
        var _this = this;
        console.log(this.datos);
        var data = this.datos;
        var error = '';
        var hay_error = false;
        if (data.direccion.length < 6) {
            error = 'Debes ingresar una dirección de al menos 3 caracteres';
            hay_error = true;
        }
        if (((this.getCardType(data.numeroTarjeta) != "AMEX") && data.cvv.length != 3) || ((this.getCardType(data.numeroTarjeta) == "AMEX") && (data.cvv.length != 4))) {
            error = 'Debes ingresar un código de verificación de 3 caracteres';
            if (this.getCardType(data.numeroTarjeta) == "AMEX")
                error = 'Debes ingresar un código de verificación de 4 caracteres';
            hay_error = true;
            if (isNaN(data.cvv.length)) {
                error = 'Debes ingresar un código de verificación de solo números';
                hay_error = true;
            }
        }
        if (data.fecha.length < 1) {
            error = 'Debes seleccionar la fecha de vencimiento';
            hay_error = true;
        }
        if (data.numeroTarjeta.length < 1) {
            error = 'Debes ingresar el número de tarjeta de credito';
            hay_error = true;
        }
        else {
            if (this.getCardType(data.numeroTarjeta) == "") {
                error = 'El número de tarjeta es inválido';
                hay_error = true;
            }
            else {
                this.datos.tipoTarjeta = this.getCardType(data.numeroTarjeta);
            }
        }
        if (data.titular.length < 3) {
            error = 'Debes ingresar el nombre del titular de la tarjeta';
            hay_error = true;
        }
        if (!this.isAlpha(data.titular)) {
            error = 'El nombre del titular de la tarjeta solo puede contener letras';
            hay_error = true;
        }
        if (data.numDoc.length < 6) {
            error = 'El número de documento no puede tener menos de 6 caracteres';
            hay_error = true;
        }
        if (data.telefono.length < 1) {
            error = 'Debes ingresar el numero de celular del titular';
            hay_error = true;
        }
        if (hay_error) {
            this.server.showAlert('Error', error);
        }
        else {
            let user = JSON.parse(localStorage.user)
            this.datos.email = user.email;
            this.datos.uid = user.uid;
            this.datos.tipoDoc = 'CEDULA';
            this.server.addTarjeta(this.datos).then(function (data) {
                _this.server.showAlert("Tarjeta Agregada", "Tarjeta agregada con éxito");
                _this.navCtrl.navigateBack("tarjetas");
            }).catch(function (err) {
                _this.server.showAlert("Error al Agregar", "Ha ocurrido un error al agregar tarjeta");
            });
        }
    };
    AddtarjetaPage.prototype.isAlpha = function (name) {
        var regex = /[a-zA-Z]+$/;
        console.log("regex:", name, regex.test(name));
        return regex.test(name);
    };
    AddtarjetaPage.prototype.getCardType = function (cardNum) {
        if (!this.luhnCheck(cardNum)) {
            return "";
        }
        var payCardType = "";
        var regexMap = [
            { regEx: /^4[0-9]{5}/ig, cardType: "VISA" },
            { regEx: /^5[1-5][0-9]{4}/ig, cardType: "MASTERCARD" },
            { regEx: /^3[47][0-9]{3}/ig, cardType: "AMEX" },
            { regEx: /^(5[06-8]\d{4}|6\d{5})/ig, cardType: "MAESTRO" }
        ];
        for (var j = 0; j < regexMap.length; j++) {
            if (cardNum.match(regexMap[j].regEx)) {
                payCardType = regexMap[j].cardType;
                break;
            }
        }
        if (cardNum.indexOf("50") === 0 || cardNum.indexOf("60") === 0 || cardNum.indexOf("65") === 0) {
            var g = "508500-508999|606985-607984|608001-608500|652150-653149";
            var i = g.split("|");
            for (var d = 0; d < i.length; d++) {
                var c = parseInt(i[d].split("-")[0], 10);
                var f = parseInt(i[d].split("-")[1], 10);
                if ((cardNum.substr(0, 6) >= c && cardNum.substr(0, 6) <= f) && cardNum.length >= 6) {
                    payCardType = "RUPAY";
                    break;
                }
            }
        }
        return payCardType;
    };
    AddtarjetaPage.prototype.luhnCheck = function (cardNum) {
        var numericDashRegex = /^[\d\-\s]+$/;
        if (!numericDashRegex.test(cardNum))
            return false;
        var nCheck = 0, nDigit = 0, bEven = false;
        var strippedField = cardNum.replace(/\D/g, "");
        for (var n = strippedField.length - 1; n >= 0; n--) {
            var cDigit = strippedField.charAt(n);
            nDigit = parseInt(cDigit, 10);
            if (bEven) {
                if ((nDigit *= 2) > 9)
                    nDigit -= 9;
            }
            nCheck += nDigit;
            bEven = !bEven;
        }
        return (nCheck % 10) === 0;
    };
    AddtarjetaPage = tslib_1.__decorate([
        Component({
            selector: 'app-addtarjeta',
            templateUrl: './addtarjeta.page.html',
            styleUrls: ['./addtarjeta.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [AuthService,
            MenuController,
            ServerService,
            NavController,
            UserService,
            NavController])
    ], AddtarjetaPage);
    return AddtarjetaPage;
}());
export { AddtarjetaPage };
//# sourceMappingURL=addtarjeta.page.js.map