import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user-service';
var TarjetasPage = /** @class */ (function () {
    function TarjetasPage(navCtrl, auth, user) {
        this.navCtrl = navCtrl;
        this.auth = auth;
        this.user = user;
        this.fromCartNav = false;
    }
    TarjetasPage.prototype.ngOnInit = function () {
        var fromCart = localStorage.getItem("fromCart");
        if (fromCart == "true") {
            this.fromCartNav = true;
        }
        this.getTarjeta();
    };
    TarjetasPage.prototype.goBack = function () {
        this.navCtrl.navigateBack("micuenta");
    };
    TarjetasPage.prototype.goAddTarjeta = function () {
        this.navCtrl.navigateForward("addtarjeta");
    };
    TarjetasPage.prototype.Selection = function (item) {
        this.tarjetas.forEach(function (x) { x.checked = false; });
        localStorage.setItem("tarjeta_actual", item.id);
        localStorage.setItem("tarjeta_nombre", item.ultimos + "|" + item.tipoTarjeta);
        var fromCart = localStorage.getItem("fromCart");
        if (fromCart == "true") {
            this.navCtrl.navigateBack("payload-checkout");
        }
    };
    TarjetasPage.prototype.getTarjeta = function () {
        var _this = this;
        //var uid = this.auth.getUser().uid;
        var uid = localStorage.uid;
        this.auth.getGen('Tarjeta/' + uid).then(function (data) {
            _this.tarjetas = data;
            var actualtarjeta = localStorage.getItem("tarjeta_actual") || 0;
            console.log("Tarjeta Actual", actualtarjeta);
            for (var i = 0; i < _this.tarjetas.length; i++) {
                if (_this.tarjetas[i].id == actualtarjeta) {
                    _this.tarjetas[i].checked = true;
                }
                else {
                    _this.tarjetas[i].checked = false;
                }
            }
        });
    };
    TarjetasPage = tslib_1.__decorate([
        Component({
            selector: 'app-tarjetas',
            templateUrl: './tarjetas.page.html',
            styleUrls: ['./tarjetas.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController, AuthService, UserService])
    ], TarjetasPage);
    return TarjetasPage;
}());
export { TarjetasPage };
//# sourceMappingURL=tarjetas.page.js.map