import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AyudaPage } from './ayuda.page';
var routes = [
    {
        path: '',
        component: AyudaPage
    }
];
var AyudaPageModule = /** @class */ (function () {
    function AyudaPageModule() {
    }
    AyudaPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [AyudaPage]
        })
    ], AyudaPageModule);
    return AyudaPageModule;
}());
export { AyudaPageModule };
//# sourceMappingURL=ayuda.module.js.map