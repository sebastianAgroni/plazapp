import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { HTTP } from '@ionic-native/http/ngx';
var TerminosPage = /** @class */ (function () {
    function TerminosPage(navCtrl, auth, http) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.auth = auth;
        this.http = http;
        this.terminos = "";
        this.terminos = localStorage.getItem("terminos");
        if (!this.terminos) {
            var url = this.auth.endpoint + '/api/terminos';
            var datos = {};
            this.http.post(url, datos, {})
                .then(function (res) {
                var resp = res;
                resp = JSON.parse(resp._body);
                console.log(resp);
                if (resp.ok) {
                    _this.terminos = resp.terminos;
                    localStorage.setItem("terminos", _this.terminos);
                }
                else {
                }
            }, function (error) {
            });
        }
    }
    TerminosPage.prototype.ngOnInit = function () {
    };
    TerminosPage.prototype.goIntro = function () {
        this.navCtrl.navigateBack('intro');
    };
    TerminosPage = tslib_1.__decorate([
        Component({
            selector: 'app-terminos',
            templateUrl: './terminos.page.html',
            styleUrls: ['./terminos.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController, AuthService, HTTP])
    ], TerminosPage);
    return TerminosPage;
}());
export { TerminosPage };
//# sourceMappingURL=terminos.page.js.map