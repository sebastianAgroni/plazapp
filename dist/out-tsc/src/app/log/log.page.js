import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, Platform, AlertController } from '@ionic/angular';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { FormBuilder } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { ServerService } from '../server/server.service';
var LogPage = /** @class */ (function () {
    function LogPage(screenOrientation, platform, navCtrl, keyboard, auth, fb, server, alertCtrl) {
        this.screenOrientation = screenOrientation;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.keyboard = keyboard;
        this.auth = auth;
        this.fb = fb;
        this.server = server;
        this.alertCtrl = alertCtrl;
        if (this.platform.is('cordova')) {
            this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        }
    }
    LogPage.prototype.ngOnInit = function () { };
    LogPage.prototype.login = function () {
        var _this = this;
        this.server.presentLoadingDefault("Autenticando Usuario...");
        var datos = { email: this.email, password: this.password };
        this.auth.doLogin(datos).then(function (resp) {
            console.log("Autenticado:", resp);
            _this.server.dismissLoading();
            _this.navCtrl.navigateRoot("tabs/home");
        }).catch(function (err) {
            _this.server.dismissLoading();
            _this.showAlert('Error al Ingresar', 'Datos inválidos o cuenta no registrada, intente nuevamente!');
            console.log("error:", err);
        });
    };
    LogPage.prototype.fbLogin = function () {
        var _this = this;
        this.server.presentLoadingDefault("Autenticando desde Facebook...");
        console.log("Facebook Login");
        this.auth.doFacebookLogin().then(function (resp) {
            console.log("Autenticado:", resp);
            _this.server.dismissLoading();
            _this.navCtrl.navigateRoot("tabs/home");
        }).catch(function (err) {
            _this.server.dismissLoading();
            _this.showAlert("Error al Ingresar", "Error al ingresar con Facebook, intente nuevamente.");
            console.log("error:", err);
        });
    };
    LogPage.prototype.googleLogin = function () {
        var _this = this;
        this.server.presentLoadingDefault("Autenticando desde Google...");
        console.log("Google Login");
        this.auth.doGoogleLogin().then(function (resp) {
            console.log("Autenticado:", resp);
            _this.server.dismissLoading();
            _this.navCtrl.navigateRoot("tabs/home");
        }).catch(function (err) {
            _this.server.dismissLoading();
            _this.showAlert("Error al Ingresar", "Error al ingresar con Google, intente nuevamente.");
            console.log("error:", err);
        });
    };
    LogPage.prototype.goRepass = function () {
        this.navCtrl.navigateForward("repass");
    };
    LogPage.prototype.goReg = function () {
        this.navCtrl.navigateRoot("registro");
    };
    LogPage.prototype.goTerminos = function () {
        this.navCtrl.navigateForward('terminos');
    };
    LogPage.prototype.goIntro = function () {
        this.navCtrl.navigateBack('intro');
    };
    LogPage.prototype.goHome = function () {
        this.navCtrl.navigateBack('tabs/home');
    };
    LogPage.prototype.showAlert = function (titulo, mensaje) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alertct;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: titulo,
                            message: mensaje,
                            buttons: ['OK']
                        })];
                    case 1:
                        alertct = _a.sent();
                        return [4 /*yield*/, alertct.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LogPage = tslib_1.__decorate([
        Component({
            selector: 'app-log',
            templateUrl: './log.page.html',
            styleUrls: ['./log.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [ScreenOrientation,
            Platform,
            NavController,
            Keyboard,
            AuthService,
            FormBuilder,
            ServerService,
            AlertController])
    ], LogPage);
    return LogPage;
}());
export { LogPage };
//# sourceMappingURL=log.page.js.map