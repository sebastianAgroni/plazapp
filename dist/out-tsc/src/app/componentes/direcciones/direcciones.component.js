import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, AlertController, MenuController } from '@ionic/angular';
import { AuthService } from '../../services/auth.service';
import { ServerService } from "../../server/server.service";
import { UserService } from '../../services/user-service';
var DireccionesComponent = /** @class */ (function () {
    function DireccionesComponent(alertCtrl, auth, menuCtrl, server, nav, userService, navCtrl) {
        var _this = this;
        this.alertCtrl = alertCtrl;
        this.auth = auth;
        this.menuCtrl = menuCtrl;
        this.server = server;
        this.nav = nav;
        this.userService = userService;
        this.navCtrl = navCtrl;
        this.address = "0";
        this.direccion = {
            tienda: "",
            dir: "",
            adicional: "",
            ciudad: {
                nombre: "Bogotá D.C",
                id: "1"
            },
            predeterminada: 0,
            lat: 0,
            lng: 0,
            uid: "",
            id: 0
        };
        this.userService.getCurrentUser()
            .then(function (user) {
            _this.auth.getGenKeyVal("Envio", "uid", user.uid).then(function (data) {
                _this.direcciones = data;
                localStorage.setItem("list-address", JSON.stringify(data));
                console.log("Direcciones: ", data);
            });
        });
        var pre_address = localStorage.getItem("address");
        if (pre_address != null) {
            this.address = pre_address;
        }
    }
    DireccionesComponent.prototype.ngOnInit = function () { };
    DireccionesComponent.prototype.checkAddress = function (event) {
        if (this.address == "-1") {
            this.presentAlertAddress();
        }
        else {
            localStorage.setItem("address", this.address);
        }
    };
    DireccionesComponent.prototype.isValid = function (dir, other) {
        if (dir.length > 4) {
            return true;
        }
        else {
            return false;
        }
    };
    DireccionesComponent.prototype.getdata = function () {
        console.log("ok");
    };
    DireccionesComponent.prototype.presentAlertAddress = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Añadir Dirección',
                            message: 'Ciudad: <select class="select-city" id="city"><option value="1">Bogotá</option></select>',
                            inputs: [
                                {
                                    name: 'direccion',
                                    placeholder: 'Dirección de Entrega'
                                },
                                {
                                    name: 'more',
                                    placeholder: 'Información Adicional'
                                }
                            ],
                            buttons: [
                                {
                                    text: 'Cancelar',
                                    role: 'cancelar',
                                    handler: function (data) {
                                        console.log('Cancel clicked');
                                        _this.address = "0";
                                    }
                                },
                                {
                                    text: 'Verificar',
                                    handler: function (data) {
                                        var city = document.getElementById("city").value;
                                        if (_this.isValid(data.direccion, data.more)) {
                                            _this.direccion.dir = data.direccion;
                                            _this.direccion.adicional = data.more;
                                            var user = _this.auth.getUser();
                                            _this.direccion.uid = user.uid;
                                            _this.server.addDireccion(_this.direccion).then(function (data) {
                                                if (data.ok) {
                                                    _this.server.showAlert("Dirección Agregada", "Dirección Agregada con éxito");
                                                    _this.address.open();
                                                    _this.nav.pop();
                                                }
                                                else {
                                                    _this.server.showAlert("Error Dirección", data.error_msg);
                                                }
                                            }).catch(function (err) {
                                                _this.server.showAlert("Error al Agregar", "Ha ocurrido un error al agregar la dirección");
                                            });
                                        }
                                        else {
                                            console.log("no added address");
                                            return false;
                                        }
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DireccionesComponent = tslib_1.__decorate([
        Component({
            selector: 'app-direcciones',
            templateUrl: './direcciones.component.html',
            styleUrls: ['./direcciones.component.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [AlertController,
            AuthService,
            MenuController,
            ServerService,
            NavController,
            UserService,
            NavController])
    ], DireccionesComponent);
    return DireccionesComponent;
}());
export { DireccionesComponent };
//# sourceMappingURL=direcciones.component.js.map