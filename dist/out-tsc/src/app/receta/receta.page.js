import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ParametrosService } from '../services/parametros.service';
var RecetaPage = /** @class */ (function () {
    function RecetaPage(navCtrl, param) {
        this.navCtrl = navCtrl;
        this.param = param;
    }
    RecetaPage.prototype.ngOnInit = function () {
    };
    RecetaPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.param.serviceData
            .subscribe(function (post) { return (_this.postid = post); });
        console.log("Recibiendo postid: ", this.postid);
        this.post = JSON.parse(localStorage.getItem('recetas'));
        this.post = this.post.filter(function (item) { return item.id === _this.postid; });
    };
    RecetaPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    RecetaPage = tslib_1.__decorate([
        Component({
            selector: 'receta',
            templateUrl: './receta.page.html',
            styleUrls: ['./receta.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController, ParametrosService])
    ], RecetaPage);
    return RecetaPage;
}());
export { RecetaPage };
//# sourceMappingURL=receta.page.js.map