import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CONTACTOPage } from './contacto.page';
var routes = [
    {
        path: '',
        component: CONTACTOPage
    }
];
var CONTACTOPageModule = /** @class */ (function () {
    function CONTACTOPageModule() {
    }
    CONTACTOPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [CONTACTOPage]
        })
    ], CONTACTOPageModule);
    return CONTACTOPageModule;
}());
export { CONTACTOPageModule };
//# sourceMappingURL=contacto.module.js.map