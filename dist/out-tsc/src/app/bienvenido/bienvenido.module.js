import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { BienvenidoPage } from './bienvenido.page';
var routes = [
    {
        path: '',
        component: BienvenidoPage
    }
];
var BienvenidoPageModule = /** @class */ (function () {
    function BienvenidoPageModule() {
    }
    BienvenidoPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [BienvenidoPage]
        })
    ], BienvenidoPageModule);
    return BienvenidoPageModule;
}());
export { BienvenidoPageModule };
//# sourceMappingURL=bienvenido.module.js.map